import React from 'react';

import styled from 'styled-components';

import {
  TileBase, TileTitle, TileSubTitle, TileSubTitleContainer, TileTitleContainer
} from "./TileBase";
import { RowContainer } from "../../basic_layout/Container";


export const ButtonTile = (props) => {
  const title = props.title;
  const subTitle = props.subTitle;
  const onClick = props.onClick;
  return (
    <TileBase onClick={ onClick }>
      <TileTitleContainer>
        <TileTitle>{ title }</TileTitle>
      </TileTitleContainer>
      <TileSubTitleContainer>
        <TileSubTitle>{ subTitle }</TileSubTitle>
      </TileSubTitleContainer>
    </TileBase>
  );
};
