import React from 'react';

import styled from 'styled-components';

import { DefaultTheme } from "../../theme/DefaultTheme";


export const OverlayDialogBox = styled.div`
  margin: auto;
  background-color: white;
  border: solid thin;
  max-width: 33%;
  text-align: left;
`;
