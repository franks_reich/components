export const OverlayBackgroundTheme = {
  normal: {
    margin: 'auto',
    display: 'flex',
    justifyContent: 'center',
    position: {
      type: 'fixed',
      width: '100%',
      height: '100%',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0
    },
    color: {
      background: 'rgba(0, 0, 0, 0.5)'
    }
  }
};
