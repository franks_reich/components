import React from 'react';

import styled from "styled-components";


export const PageRowContainer = styled.div`
  display: flex;
  flex-direction: row;
  padding: 1em 1em 0;
  align-items: flex-start;
`;

export const RowContainer = styled.div`
  display: flex;
  flex-direction: row;
  padding: 1em 1em 0;
  align-items: stretch;
`;

export const CenterRowContainer = styled(RowContainer)`
  text-align: center;
`;

export const ColumnContainer = styled.div`
  display: flex;
  flex-direction: column;
  padding: 1em 1em 0;
`;

export const PaddingContainer = styled.div`
  padding: ${ props => props.padding };
  display: flex;
  align-items: stretch;
  flex-direction: column;
  text-align: center;
`;

PaddingContainer.defaultProps = { padding: '1em' };

export const FixedWidthDiv = styled.div`
  width: ${ props => props.width };
`;

export const GrowingDiv = styled.div`
  min-width: 1em;
  flex-grow: 1;
  flex-basis: auto;
`;

export const FixedHeightDiv = styled.div`
  height: ${ props => props.height };
`;

export const Spacer = (props) => {
  const width = props.width;
  const height = props.height;
  if (width) {
    return <FixedWidthDiv width={ width } />;
  } else if (height) {
    return <FixedHeightDiv height={ height } />
  } else {
    return <GrowingDiv />;
  }
};
