import React from 'react';

import styled from "styled-components";


export const OverlayPaneContainer = styled.div`
  position: fixed;
  top: ${ props => props.position.top };
  left: ${ props => props.position.left };
  bottom: ${ props => props.position.bottom ? props.position.bottom : "unset" };
  right: ${ props => props.position.right ? props.position.right : "unset" };
  width: ${ props => props.position.width ? props.position.width : "unset" };
  box-shadow: 0.1em 0.1em 0.5em darkgrey;
  z-index: -1;
`;
