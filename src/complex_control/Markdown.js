import React from 'react';

import ReactMarkdown from "react-markdown/with-html";

import { MarkdownContainer } from "./markdown/MarkdownContainer";
import { HeadingRenderer } from "./markdown/renderer/HeadingRenderer";
import { ParagraphRenderer } from "./markdown/renderer/ParagraphRenderer";

import {
  TableRenderer, RowRenderer, TableCellRenderer
} from "./markdown/renderer/TableRenderers";

import {
  ListRenderer, ListItemRenderer
} from "./markdown/renderer/ListRenderers";

import { CodeBlockRenderer } from "./markdown/renderer/CodeBlockRenderer";


const customRenderers = {
  paragraph: ParagraphRenderer,
  heading: HeadingRenderer,
  table: TableRenderer,
  tableRow: RowRenderer,
  tableCell: TableCellRenderer,
  list: ListRenderer,
  listItem: ListItemRenderer,
  code: CodeBlockRenderer
};

export const Markdown = (props) => {
  const source = props.source;
  const escapeHtml = props.escapeHtml;
  return (
    <MarkdownContainer>
      <ReactMarkdown
        renderers={ customRenderers }
        source={ source }
        escapeHtml={ escapeHtml } />
    </MarkdownContainer>
  );
};
