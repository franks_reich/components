export const ApplicationHighlightTitleTheme = {
  font: '"Montserrat",sans-serif',
  weight: 'lighter',
  fontStyle: 'normal',
  size: '2em',
  color: {
    background: 'transparent',
    foreground: 'blue'
  },
};
