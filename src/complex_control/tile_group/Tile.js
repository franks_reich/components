import React from 'react';

import { NavigationTile } from "./NavigationTile";
import { ButtonTile } from "./ButtonTile";


export const Tile = (props) => {
  const type = props.tile.type;
  const title = props.tile.title;
  const subTitle = props.tile.subTitle;
  if (type === "Button") {
    const onClick = props.tile.onClick;
    return (
      <ButtonTile
        title={ title }
        subTitle={ subTitle }
        onClick={ onClick } />);
  } else if (type === "Navigation") {
    const url = props.tile.url;
    const navType = props.tile.navType;
    return (
      <NavigationTile
        title={ title }
        subTitle={ subTitle }
        navType={ navType }
        url={ url } />);
  } else {
    return null;
  }
};
