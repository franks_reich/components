import React from 'react';

import { curry } from 'ramda';
import styled from "styled-components";

import { RowContainer, Spacer } from "../../basic_layout/Container";
import { ItemSubTitle } from "../../basic_layout/Text";
import { IconItemSubTitle } from "../../basic_control/Icon";
import { ButtonBase } from "../../basic_control/Button";
import { DefaultTheme } from "../../theme/DefaultTheme";
import { HeaderLink } from "./ApplicationHeaderLink";
import { OverlayMenu } from "../OverlayMenu";


const StyledMenuButton = styled.button`
  margin: 0;
  padding: 0;
  background-color: ${ props => props.theme.button.normal.color.background };
  border: none;
  border-color: burlywood;
  transition-duration: ${ props => props.theme.button.normal.transition.duration };
  border-bottom: none;
  
  &:hover {
    background-color: rgba(1, 1, 1, 0.1);
    border: none;
    border-color: burlywood;
    box-shadow: 0.1em 0.1em 0.5em darkgrey;
    transition-duration: ${ props => props.theme.button.hover.transition.duration };
    border-bottom: none;
  }
  
  &:active {
    background-color: rgba(1, 1, 1, 0.2);
    border: none;
    box-shadow: ${ props => props.theme.button.active.shadow };
    transition-duration: ${ props => props.theme.button.active.transition.duration };
    border-bottom: none;
  }
  
  &:focus {
    outline: 0;
  }
`;

StyledMenuButton.defaultProps = { theme: DefaultTheme };

const HeaderMenuButton = curry(ButtonBase)(
  StyledMenuButton, IconItemSubTitle, ItemSubTitle);

export class ApplicationHeaderMenu extends React.Component {
  render() {
    const menu = this.props.menu;
    const buttons = this.createHeaderButtons(menu);
    return (
      <RowContainer>
        { buttons }
      </RowContainer>
    )
  }

  createHeaderButtons(menu) {
    return menu.map((item, index) => {
      const calculatedIndex = index * 2;
      const spacer = this.createSpacer(menu, item, index, calculatedIndex);
      const button = this.createHeaderButton(item, calculatedIndex + 1);
      return ([ spacer, button ]);
    }).flat();
  }

  createHeaderButton(item, index) {
    if (item.type === "Navigation") {
      return (
        <HeaderLink
          to={ item.url }
          key={ index }>
          <HeaderMenuButton
            text={ item.title } />
        </HeaderLink>);
    } else if (item.type === "Button") {
      return (
        <HeaderMenuButton
          onClick={ item.onClick }
          text={ item.title }
          key={ index } />);
    } else if (item.type === "Menu") {
      return (
        <OverlayMenu
          fixed={ true }
          button={ HeaderMenuButton }
          text={ item.title }
          key={ index }
          items={ item.items } />);
    } else {
      return null;
    }
  }

  createSpacer(menu, item, index, key) {
    if (index !== menu.length) {
      return <Spacer width="0.5em" key={ key } />
    }
    return null;
  }
}
