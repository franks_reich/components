import React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components'

import { RowContainer, ColumnContainer, PaddingContainer, Spacer } from '../../src';
import { GrowingDiv, FixedHeightDiv, FixedWidthDiv } from '../../src/basic_layout/Container';


test(
  'The row container should be a div with display: flex, flex-direction: row, ' +
  'padding-top: 1em, padding-left: 1em and padding-bottom: 0em', () => {
    const testRenderer = renderer.create(<RowContainer/>);
    const tree = testRenderer.toJSON();
    expect(tree).toMatchSnapshot();
    expect(tree).toHaveStyleRule('display', 'flex');
    expect(tree).toHaveStyleRule('flex-direction', 'row');
    expect(tree).toHaveStyleRule('padding', '1em 1em 0');
    expect(tree).toHaveStyleRule('align-items', 'stretch');
    testRenderer.root.findByType('div');
  });

test(
  'The column container should be a div with display: flex, flex-direction: column ' +
  'padding-top: 1em, padding-left: 1em and padding-bottom: 0em', () => {
    const testRenderer = renderer.create(<ColumnContainer/>);
    const tree = testRenderer.toJSON();
    expect(tree).toMatchSnapshot();
    expect(tree).toHaveStyleRule('display', 'flex');
    expect(tree).toHaveStyleRule('flex-direction', 'column');
    expect(tree).toHaveStyleRule('padding', '1em 1em 0');
    testRenderer.root.findByType('div');
  });

describe('The padding container', () => {
  it('should have a div as base', () => {
    const testRenderer = renderer.create(<PaddingContainer />);
    testRenderer.root.findByType('div');
  });

  it('should have 1em padding by default', () => {
    const testRenderer = renderer.create(<PaddingContainer />);
    const tree = testRenderer.toJSON();
    expect(tree).toHaveStyleRule('padding', '1em');
  });

  it('should allow to change the padding', () => {
    const testRenderer = renderer.create(<PaddingContainer padding='2em' />);
    const tree = testRenderer.toJSON();
    expect(tree).toHaveStyleRule('padding', '2em');
  });
});

describe('The spacer', () => {
  it('should have a div as base', () => {
    const testRenderer = renderer.create(<Spacer />);
    testRenderer.root.findByType('div');
  });

  it('should be a growing div by default', () => {
    const testRenderer = renderer.create(<Spacer />);
    testRenderer.root.findByType(GrowingDiv);
  });

  it('should have flex-grow: 1 if it is a growing div', () => {
    const testRenderer = renderer.create(<Spacer />);
    const tree = testRenderer.toJSON();
    expect(tree).toHaveStyleRule('flex-grow', '1');
  });

  it('should have min-width: 1em if it is a growing div', () => {
    const testRenderer = renderer.create(<Spacer />);
    const tree = testRenderer.toJSON();
    expect(tree).toHaveStyleRule('min-width', '1em');
  });

  it('should have flex-basis: auto if it is a growing div', () => {
    const testRenderer = renderer.create(<Spacer />);
    const tree = testRenderer.toJSON();
    expect(tree).toHaveStyleRule('flex-basis', 'auto');
  });

  it('should be a fixed width div if the width is set', () => {
    const testRenderer = renderer.create(<Spacer width='1em' />);
    testRenderer.root.findByType(FixedWidthDiv);
  });

  it('should allow to set the width', () => {
    const testRenderer = renderer.create(<Spacer width='1em' />);
    const tree = testRenderer.toJSON();
    expect(tree).toHaveStyleRule('width', '1em');
  });

  it('should be a fixed height div if the height is set', () => {
    const testRenderer = renderer.create(<Spacer height='1em' />);
    testRenderer.root.findByType(FixedHeightDiv);
  });

  it('should allow to set the height', () => {
    const testRenderer = renderer.create(<Spacer height='1em' />);
    const tree = testRenderer.toJSON();
    expect(tree).toHaveStyleRule('height', '1em');
  });
});
