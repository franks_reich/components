import React from 'react';

import { OverlayBackground } from "./overlay/OverlayBackground";
import { BusyOverlayIcon } from "./overlay/BusyOverlayIcon"


export const BusyOverlay = () => (
  <OverlayBackground>
    <BusyOverlayIcon className="fas fa-spinner" />
  </OverlayBackground>
);
