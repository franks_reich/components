import React from 'react';

import { ContentContainer } from "../MarkdownContainer";
import {
  Table, Row, HeaderRow, Header, Field
} from "../../../basic_layout/Table";


export const TableRenderer = (props) => {
  const children = props.children;
  return (
    <ContentContainer>
      <Table>
        { children }
      </Table>
    </ContentContainer>
  );
};

export const RowRenderer = (props) => {
  const children = props.children;
  const isHeader = props.isHeader;

  if (isHeader) {
    return (
      <HeaderRow>
        { children }
      </HeaderRow>
    );
  } else {
    return (
      <Row>
        {children}
      </Row>
    );
  }
};

export const TableCellRenderer = (props) => {
  const children = props.children;
  const isHeader = props.isHeader;
  if (isHeader) {
    return <Header>{ children }</Header>;
  } else {
    return <Field>{ children }</Field>
  }
};
