import React from 'react';

import styled from 'styled-components';


export const OrderedList = styled.ol`
  scroll-padding-inline-start: 1.5em;
`;

export const UnorderedList = styled.ul`
  scroll-padding-inline-start: 1.5em;
`;

export const ListItem = styled.li`
  font-family: "Montserrat", sans-serif;
  font-weight: lighter;
  font-style: normal;
  font-size: 0.8em;
  background-color: transparent;
  color: black;
`;
