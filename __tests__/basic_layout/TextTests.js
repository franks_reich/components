import React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components'

import {
  ApplicationTitle, ApplicationHighlightTitle, ComponentTitle, ComponentHighlightTitle,
  ComponentSubTitle, ComponentHighlightSubTitle, DefaultTheme, ItemTitle, ItemHighlightTitle,
  ItemSubTitle, ItemHighlightSubTitle, Text, HighlightText, BoldText
} from "../../src";


const defaultThemeTests = (tree) => {
  expect(tree).toHaveStyleRule('background-color', DefaultTheme.text.color.background);
  expect(tree).toHaveStyleRule('color', DefaultTheme.text.color.foreground);
  expect(tree).toHaveStyleRule('font-family', DefaultTheme.text.font);
  expect(tree).toHaveStyleRule('font-weight', DefaultTheme.text.weight);
  expect(tree).toHaveStyleRule('margin', '0');
  expect(tree).toHaveStyleRule('padding', '0');
};

const highlightThemeTests = (tree) => {
  expect(tree).toHaveStyleRule('background-color', DefaultTheme.text.color.background);
  expect(tree).toHaveStyleRule('color', DefaultTheme.text.color.highlight);
  expect(tree).toHaveStyleRule('font-family', DefaultTheme.text.font);
  expect(tree).toHaveStyleRule('font-weight', DefaultTheme.text.weight);
  expect(tree).toHaveStyleRule('margin', '0');
  expect(tree).toHaveStyleRule('padding', '0');
};

const defaultTextTests = (testRenderer, baseType, fontSize, text) => {
  const tree = testRenderer.toJSON();
  expect(tree).toMatchSnapshot();
  expect(tree).toHaveStyleRule('font-size', fontSize);
  defaultThemeTests(tree);
  const tagInstance = testRenderer.root.findByType(baseType);
  const textInstance = tagInstance.children[0];
  expect(textInstance).toBe(text);
};

const highlightTitleTests = (testRenderer, baseType, fontSize, text) => {
  const tree = testRenderer.toJSON();
  expect(tree).toMatchSnapshot();
  expect(tree).toHaveStyleRule('font-size', fontSize);
  highlightThemeTests(tree);
  const tagInstance = testRenderer.root.findByType(baseType);
  const textInstance = tagInstance.children[0];
  expect(textInstance).toBe(text);
};

const highlightTextTests = (testRenderer, baseType, fontSize, text) => {
  const tree = testRenderer.toJSON();
  expect(tree).toMatchSnapshot();
  expect(tree).toHaveStyleRule('font-size', fontSize);
  expect(tree).toHaveStyleRule('font-weight', 'lighter');
  expect(tree).toHaveStyleRule('font-style', 'italic');
  expect(tree).toHaveStyleRule('background-color', DefaultTheme.text.color.background);
  expect(tree).toHaveStyleRule('color', DefaultTheme.text.color.foreground);
  expect(tree).toHaveStyleRule('font-family', DefaultTheme.text.font);
  expect(tree).toHaveStyleRule('margin', '0');
  expect(tree).toHaveStyleRule('padding', '0');
  const tagInstance = testRenderer.root.findByType(baseType);
  const textInstance = tagInstance.children[0];
  expect(textInstance).toBe(text);
};

const boldTextTests = (testRenderer, baseType, fontSize, text) => {
  const tree = testRenderer.toJSON();
  expect(tree).toMatchSnapshot();
  expect(tree).toHaveStyleRule('font-size', fontSize);
  expect(tree).toHaveStyleRule('font-weight', 'bold');
  expect(tree).toHaveStyleRule('background-color', DefaultTheme.text.color.background);
  expect(tree).toHaveStyleRule('color', DefaultTheme.text.color.foreground);
  expect(tree).toHaveStyleRule('font-family', DefaultTheme.text.font);
  expect(tree).toHaveStyleRule('margin', '0');
  expect(tree).toHaveStyleRule('padding', '0');
  const tagInstance = testRenderer.root.findByType(baseType);
  const textInstance = tagInstance.children[0];
  expect(textInstance).toBe(text);
};

describe('The Application Title', () => {
  it('should be a h1 styled with the default theme', () => {
    const testRenderer = renderer.create(<ApplicationTitle>Test Title</ApplicationTitle>);
    defaultTextTests(testRenderer, 'h1', '2.2em', "Test Title");
  });

  it('should align itself to the center in a flex container', () => {
    const testRenderer = renderer.create(<ApplicationTitle>Test Title</ApplicationTitle>);
    const tree = testRenderer.toJSON();
    expect(tree).toHaveStyleRule('align-self', 'center');
  });
});

describe('The ApplicationHighlightTitle', () => {
  it('should be a h1 styled with the default theme using the highlight color', () => {
    const testRenderer = renderer.create(
      <ApplicationHighlightTitle>Test Title</ApplicationHighlightTitle>);
    highlightTitleTests(testRenderer, 'h1', '2.2em', "Test Title");
  });

  it('should align itself to the center in a flex container', () => {
    const testRenderer = renderer.create(
      <ApplicationHighlightTitle>Test Title</ApplicationHighlightTitle>);
    const tree = testRenderer.toJSON();
    expect(tree).toHaveStyleRule('align-self', 'center');
  });
});

describe('The ComponentTitle', () => {
  it('should be a h2 styled with the default theme', () => {
    const testRenderer = renderer.create(<ComponentTitle>Test Title</ComponentTitle>);
    defaultTextTests(testRenderer, 'h2', '2em', "Test Title");
  });

  it('should align itself to the center in a flex container', () => {
    const testRenderer = renderer.create(<ComponentTitle>Test Title</ComponentTitle>);
    const tree = testRenderer.toJSON();
    expect(tree).toHaveStyleRule('align-self', 'center');
  });
});

describe('The ComponentHighlightTitle', () => {
  it('should be a h2 styled with the default theme using the highlight color.', () => {
    const testRenderer = renderer.create(
      <ComponentHighlightTitle>Test Title</ComponentHighlightTitle>);
    highlightTitleTests(testRenderer, 'h2', '2em', "Test Title");
  });

  it('should align itself to the center in a flex container', () => {
    const testRenderer = renderer.create(
      <ComponentHighlightTitle>Test Title</ComponentHighlightTitle>);
    const tree = testRenderer.toJSON();
    expect(tree).toHaveStyleRule('align-self', 'center');
  });
});

describe('The ComponentSubTitle', () => {
  it('should be a h3 styled with the default theme', () => {
    const testRenderer = renderer.create(<ComponentSubTitle>Test Title</ComponentSubTitle>);
    defaultTextTests(testRenderer, 'h3', '1.8em', "Test Title");
  });

  it('should align itself to the center in a flex container', () => {
    const testRenderer = renderer.create(<ComponentSubTitle>Test Title</ComponentSubTitle>);
    const tree = testRenderer.toJSON();
    expect(tree).toHaveStyleRule('align-self', 'center');
  });
});

describe('The ComponentHighlighSubTitle', () => {
  it('should be a h3 styled with the default theme using the highlight color', () => {
    const testRenderer = renderer.create(
      <ComponentHighlightSubTitle>Test Title</ComponentHighlightSubTitle>);
    highlightTitleTests(testRenderer, 'h3', '1.8em', 'Test Title');
  });

  it('should align itself to the center in a flex container', () => {
    const testRenderer = renderer.create(
      <ComponentHighlightSubTitle>Test Title</ComponentHighlightSubTitle>);
    const tree = testRenderer.toJSON();
    expect(tree).toHaveStyleRule('align-self', 'center');
  });
});

describe('The ItemTitle', () => {
  it('should be a h4 styled with the default theme', () => {
    const testRenderer = renderer.create(<ItemTitle>Test Title</ItemTitle>);
    defaultTextTests(testRenderer, 'h4', '1.6em', "Test Title");
  });

  it('should align itself to the center in a flex container', () => {
    const testRenderer = renderer.create(<ItemTitle>Test Title</ItemTitle>);
    const tree = testRenderer.toJSON();
    expect(tree).toHaveStyleRule('align-self', 'center');
  });
});

describe('The ItemHighlightTitle', () => {
  it('should be a h4 styled with the default theme using the highlight color', () => {
    const testRenderer = renderer.create(<ItemHighlightTitle>Test Title</ItemHighlightTitle>);
    highlightTitleTests(testRenderer, 'h4', '1.6em', 'Test Title');
  });

  it('should align itself to the center in a flex container', () => {
    const testRenderer = renderer.create(<ItemHighlightTitle>Test Title</ItemHighlightTitle>);
    const tree = testRenderer.toJSON();
    expect(tree).toHaveStyleRule('align-self', 'center');
  });
});

describe('The ItemSubTitle', () => {
  it('should be a h5 styled with the default theme', () => {
    const testRenderer = renderer.create(<ItemSubTitle>Test Title</ItemSubTitle>);
    defaultTextTests(testRenderer, 'h5', '1.4em', "Test Title");
  });

  it('should align itself to the center in a flex container', () => {
    const testRenderer = renderer.create(<ItemSubTitle>Test Title</ItemSubTitle>);
    const tree = testRenderer.toJSON();
    expect(tree).toHaveStyleRule('align-self', 'center');
  });
});

describe('The ItemHighlightSubTitle', () => {
  it('should be a h5 styled with the default theme using the highlight color', () => {
    const testRenderer = renderer.create(
      <ItemHighlightSubTitle>Test Title</ItemHighlightSubTitle>);
    highlightTitleTests(testRenderer, 'h5', '1.4em', 'Test Title');
  });

  it('should align itself to the center in a flex container', () => {
    const testRenderer = renderer.create(
      <ItemHighlightSubTitle>Test Title</ItemHighlightSubTitle>);
    const tree = testRenderer.toJSON();
    expect(tree).toHaveStyleRule('align-self', 'center');
  });
});

test('The Text should be a p styled with the default theme', () => {
  const testRenderer = renderer.create(<Text>Test Title</Text>);
  defaultTextTests(testRenderer, 'p', '1em', "Test Title");
});

test(
  'The HighlightText should be a p styled with the default theme and ' +
  'font-style italic', () => {
    const testRenderer = renderer.create(<HighlightText>Test Text</HighlightText>);
    highlightTextTests(testRenderer, 'p', '1em', 'Test Text');
  });

test('The BoldText should be a p styled with the default theme and ' +
  'font-weight bold', () => {
  const testRenderer = renderer.create(<BoldText>Test Text</BoldText>);
  boldTextTests(testRenderer, 'p', '1em', 'Test Text');
});
