export const NormalTextTheme = {
  font: '"Montserrat",sans-serif',
  weight: 'lighter',
  fontStyle: 'normal',
  size: '0.8em',
  color: {
    background: 'transparent',
    foreground: 'black'
  }
};
