import React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components'

import { Table, Row, Header, Field } from '../../src';
import { DefaultTheme } from "../../src";


test(
  'The Table should be a table with flex-grow: 1, border: solid thin, border-collapse, ' +
  'margin-bottom: 1em and font-familiy and font-size from the default theme', () => {
    const testRenderer = renderer.create(<Table/>);
    const tree = testRenderer.toJSON();
    expect(tree).toHaveStyleRule('flex-grow', '1');
    expect(tree).toHaveStyleRule('border', 'solid thin');
    expect(tree).toHaveStyleRule('border-collapse', 'collapse');
    expect(tree).toHaveStyleRule('font-family', DefaultTheme.text.font);
    expect(tree).toHaveStyleRule('font-size', DefaultTheme.text.size.text);
    testRenderer.root.findByType('table');
  });

test('The Row should be a tr', () => {
  const testRenderer = renderer.create(<Row/>);
  testRenderer.root.findByType('tr');
});

test('The Header should be a th with border: solid thin and font-weight: bold', () => {
  const testRenderer = renderer.create(<Header/>);
  const tree = testRenderer.toJSON();
  expect(tree).toHaveStyleRule('border', 'solid thin');
  expect(tree).toHaveStyleRule('font-weight', 'bold');
  testRenderer.root.findByType('th');
});

test(
  'The Field should be a td with border: solid thin, border-top: 0 and border-bottom: 0',
  () => {
    const testRenderer = renderer.create(<Field/>);
    const tree = testRenderer.toJSON();
    expect(tree).toHaveStyleRule('border', 'solid thin');
    expect(tree).toHaveStyleRule('border-bottom', '0');
    expect(tree).toHaveStyleRule('border-top', '0');
    testRenderer.root.findByType('td');
  });
