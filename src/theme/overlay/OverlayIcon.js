export const OverlayIcon = {
  normal: {
    margin: 'auto',
    size: '12em',
    color: {
      foreground: 'dimgrey'
    }
  }
};
