export const ItemSubTitleTheme = {
  font: '"Montserrat",sans-serif',
  weight: 'bold',
  fontStyle: 'normal',
  size: '0.9em',
  color: {
    background: 'transparent',
    foreground: 'black'
  }
};
