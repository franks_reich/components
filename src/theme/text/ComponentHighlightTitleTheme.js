export const ComponentHighlightTitleTheme = {
  font: '"Montserrat",sans-serif',
  weight: 'normal',
  fontStyle: 'normal',
  size: '1.6em',
  color: {
    background: 'transparent',
    foreground: 'blue'
  }
};
