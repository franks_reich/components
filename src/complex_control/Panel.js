import React from 'react';

import { curry } from 'ramda';

import { InnerPanel } from "./panel/InnerPanel";
import {
  ApplicationTitle, ComponentTitle, ComponentSubTitle,
  ItemTitle, ItemSubTitle
} from "../basic_layout/Text";
import {
  IconApplicationTitle, IconComponentTitle,
  IconComponentSubTitle, IconItemTitle, IconItemSubTitle
} from "../basic_control/Icon";


const Panel = (Title, Icon, props) => {
  return (
    <InnerPanel
      Title={ Title }
      Icon={ Icon }
      {...props} />
  );
};

export const ApplicationPanel = curry(Panel)
  (ApplicationTitle, IconApplicationTitle);
export const ComponentPanel = curry(Panel)
  (ComponentTitle, IconComponentTitle);
export const ComponentSubPanel = curry(Panel)
  (ComponentSubTitle, IconComponentSubTitle);
export const ItemPanel = curry(Panel)
  (ItemTitle, IconItemTitle);
export const ItemSubPanel = curry(Panel)
  (ItemSubTitle, IconItemSubTitle);
