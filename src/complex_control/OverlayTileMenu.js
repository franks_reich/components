import React from 'react';

import styled from 'styled-components';

import { OverlayBackground } from "./overlay/OverlayBackground";
import { OverlayTileMenuContainer } from "./overlay/OverlayTileMenuContainer";
import { ComponentTitle } from "../basic_layout/Text";
import { RowContainer, Spacer } from "../basic_layout/Container";
import { IconComponentTitle } from "../basic_control/Icon";
import { TileGroup } from "./TileGroup";


const ScrollContainer = styled.div`
`;

export class OverlayTileMenu extends React.Component {
  render() {
    const onCancelNavigation = this.props.onCancelNavigation;
    const ignoreEvent = (event) => {
      event.stopPropagation();
    };
    const tileGroups = this.createTileGroups();
    return (
      <OverlayBackground
        onClick={onCancelNavigation}>
        <OverlayTileMenuContainer
          onClick={ignoreEvent}>
          <RowContainer>
            <ComponentTitle>Menu</ComponentTitle>
            <Spacer/>
            <IconComponentTitle
              className="fas fa-times"
              onClick={onCancelNavigation}/>
          </RowContainer>
          <ScrollContainer>
            { tileGroups }
          </ScrollContainer>
        </OverlayTileMenuContainer>
      </OverlayBackground>
    );
  }

  createTileGroups() {
    return this.props.tileGroups.map((tileGroup, index) => (
      <TileGroup
        key={ index }
        title={ tileGroup.title }
        tiles={ tileGroup.tiles } />
    ));
  }
}
