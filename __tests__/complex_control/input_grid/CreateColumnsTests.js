import React from 'react';

import {
  createLabelColumn, createInputColumn, createColumn
} from "../../../src/complex_control/InputGrid";

import {
  Text
} from "../../../src/";


describe('The create label column function', () => {
  const input = [
    {
      label: 'Label 1',
      value: 'Value 1',
      callback: () => {}
    },
    {
      label: 'Label 2',
      value: 'Value 2',
      callback: () => {}
    },
    {
      label: 'Label 3',
      value: 'Value 3',
      callback: () => {}
    },
    {
      label: 'Label 4',
      value: 'Value 4',
      callback: () => {}
    }
  ];
});
