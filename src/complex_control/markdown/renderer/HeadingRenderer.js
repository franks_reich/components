import React from 'react';

import {
  ComponentTitle, ComponentSubTitle, ItemTitle, ItemSubTitle
} from "../../../basic_layout/Text";

import {
  FirstLevelContainer, SecondLevelContainer, ThirdLevelContainer,
  FourthLevelContainer
} from "../MarkdownContainer";

import { RowContainer } from "../../../basic_layout/Container";


export const HeadingRenderer = (props) => {
  const level = props.level;
  const children = props.children;
  if (level === 1) {
    return (
      <FirstLevelContainer>
        <ComponentTitle>{ children }</ComponentTitle>
      </FirstLevelContainer>
    );
  } else if (level === 2) {
    return (
      <SecondLevelContainer>
        <ComponentSubTitle>{ children }</ComponentSubTitle>
      </SecondLevelContainer>
    );
  } else if (level === 3) {
    return (
      <ThirdLevelContainer>
        <ItemTitle>{ children }</ItemTitle>
      </ThirdLevelContainer>
    );
  } else {
    return (
      <FourthLevelContainer>
        <ItemSubTitle>{ children }</ItemSubTitle>
      </FourthLevelContainer>
    );
  }
};
