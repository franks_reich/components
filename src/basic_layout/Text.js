import React from 'react';

import styled from "styled-components";

import { DefaultTheme } from "../theme/DefaultTheme";


export const ApplicationTitle = styled.h1`
  align-self: center;
  margin: 0;
  padding: 0;
  font-family: ${ props => props.theme.text.applicationTitle.font };
  font-weight: ${ props => props.theme.text.applicationTitle.weight };
  font-size: ${ props => props.theme.text.applicationTitle.size };
  font-style: ${ props => props.theme.text.applicationTitle.fontStyle };
  display: inline;
  color: ${ props => props.theme.text.applicationTitle.color.foreground };
  background-color: ${ props => props.theme.text.applicationTitle.color.background };
`;

ApplicationTitle.defaultProps = { theme: DefaultTheme };

export const ApplicationHighlightTitle = styled(ApplicationTitle)`
  align-self: center;
  margin: 0;
  padding: 0;
  font-family: ${ props => props.theme.text.applicationHighlightTitle.font };
  font-weight: ${ props => props.theme.text.applicationHighlightTitle.weight };
  font-size: ${ props => props.theme.text.applicationHighlightTitle.size };
  font-style: ${ props => props.theme.text.applicationHighlightTitle.fontStyle };
  display: inline;
  color: ${ props => props.theme.text.applicationHighlightTitle.color.foreground };
  background-color: ${ props => props.theme.text.applicationHighlightTitle.color.background };
`;

ApplicationHighlightTitle.defaultProps = { theme: DefaultTheme };

export const ComponentTitle = styled.h2`
  align-self: center;
  margin: 0;
  padding: 0;
  font-family: ${ props => props.theme.text.componentTitle.font };
  font-weight: ${ props => props.theme.text.componentTitle.weight };
  font-size: ${ props => props.theme.text.componentTitle.size };
  font-style: ${ props => props.theme.text.componentTitle.fontStyle };
  display: inline;
  color: ${ props => props.theme.text.componentTitle.color.foreground };
  background-color: ${ props => props.theme.text.componentTitle.color.background };
`;

ComponentTitle.defaultProps = { theme: DefaultTheme };

export const ComponentHighlightTitle = styled(ComponentTitle)`
  align-self: center;
  margin: 0;
  padding: 0;
  font-family: ${ props => props.theme.text.componentHighlightTitle.font };
  font-weight: ${ props => props.theme.text.componentHighlightTitle.weight };
  font-size: ${ props => props.theme.text.componentHighlightTitle.size };
  font-style: ${ props => props.theme.text.componentHighlightTitle.fontStyle };
  display: inline;
  color: ${ props => props.theme.text.componentHighlightTitle.color.foreground };
  background-color: ${ props => props.theme.text.componentHighlightTitle.color.background };
`;

ComponentHighlightTitle.defaultProps = { theme: DefaultTheme };

export const ComponentSubTitle = styled.h3`
  align-self: center;
  margin: 0;
  padding: 0;
  font-family: ${ props => props.theme.text.componentSubTitle.font };
  font-weight: ${ props => props.theme.text.componentSubTitle.weight };
  font-size: ${ props => props.theme.text.componentSubTitle.size };
  font-style: ${ props => props.theme.text.componentSubTitle.fontStyle };
  display: inline;
  color: ${ props => props.theme.text.componentSubTitle.color.foreground };
  background-color: ${ props => props.theme.text.componentSubTitle.color.background };
`;

ComponentSubTitle.defaultProps = { theme: DefaultTheme };

export const ComponentHighlightSubTitle = styled(ComponentSubTitle)`
  align-self: center;
  margin: 0;
  padding: 0;
  font-family: ${ props => props.theme.text.componentHighlightSubTitle.font };
  font-weight: ${ props => props.theme.text.componentHighlightSubTitle.weight };
  font-size: ${ props => props.theme.text.componentHighlightSubTitle.size };
  font-style: ${ props => props.theme.text.componentHighlightSubTitle.fontStyle };
  display: inline;
  color: ${ props => props.theme.text.componentHighlightSubTitle.color.foreground };
  background-color: ${ props => props.theme.text.componentHighlightSubTitle.color.background };
`;

ComponentHighlightSubTitle.defaultProps = { theme: DefaultTheme };

export const ItemTitle = styled.h4`
  align-self: center;
  margin: 0;
  padding: 0;
  font-family: ${ props => props.theme.text.itemTitle.font };
  font-weight: ${ props => props.theme.text.itemTitle.weight };
  font-size: ${ props => props.theme.text.itemTitle.size };
  font-style: ${ props => props.theme.text.itemTitle.fontStyle };
  display: inline;
  color: ${ props => props.theme.text.itemTitle.color.foreground };
  background-color: ${ props => props.theme.text.itemTitle.color.background };
`;

ItemTitle.defaultProps = { theme: DefaultTheme };

export const ItemHighlightTitle = styled(ItemTitle)`
  align-self: center;
  margin: 0;
  padding: 0;
  font-family: ${ props => props.theme.text.itemHighlightTitle.font };
  font-weight: ${ props => props.theme.text.itemHighlightTitle.weight };
  font-size: ${ props => props.theme.text.itemHighlightTitle.size };
  font-style: ${ props => props.theme.text.itemHighlightTitle.fontStyle };
  display: inline;
  color: ${ props => props.theme.text.itemHighlightTitle.color.foreground };
  background-color: ${ props => props.theme.text.itemHighlightTitle.color.background };
`;

ItemHighlightTitle.defaultProps = { theme: DefaultTheme };

export const ItemSubTitle = styled.h5`
  align-self: center;
  margin: 0;
  padding: 0;
  font-family: ${ props => props.theme.text.itemSubTitle.font };
  font-weight: ${ props => props.theme.text.itemSubTitle.weight };
  font-size: ${ props => props.theme.text.itemSubTitle.size };
  font-style: ${ props => props.theme.text.itemSubTitle.fontStyle };
  display: inline;
  color: ${ props => props.theme.text.itemSubTitle.color.foreground };
  background-color: ${ props => props.theme.text.itemSubTitle.color.background };
`;

ItemSubTitle.defaultProps = { theme: DefaultTheme };

export const ItemHighlightSubTitle = styled(ItemSubTitle)`
  align-self: center;
  margin: 0;
  padding: 0;
  font-family: ${ props => props.theme.text.itemHighlightSubTitle.font };
  font-weight: ${ props => props.theme.text.itemHighlightSubTitle.weight };
  font-size: ${ props => props.theme.text.itemHighlightSubTitle.size };
  font-style: ${ props => props.theme.text.itemHighlightSubTitle.fontStyle };
  display: inline;
  color: ${ props => props.theme.text.itemHighlightSubTitle.color.foreground };
  background-color: ${ props => props.theme.text.itemHighlightSubTitle.color.background };
`;

ItemHighlightSubTitle.defaultProps = { theme: DefaultTheme };

export const Text = styled.p`
  margin: 0;
  padding: 0;
  font-family: ${ props => props.theme.text.normalText.font };
  font-weight: ${ props => props.theme.text.normalText.weight };
  font-size: ${ props => props.theme.text.normalText.size };
  font-style: ${ props => props.theme.text.normalText.fontStyle };
  display: inline;
  color: ${ props => props.theme.text.normalText.color.foreground };
  background-color: ${ props => props.theme.text.normalText.color.background };
`;

Text.defaultProps = { theme: DefaultTheme };

export const HighlightText = styled.p`
  margin: 0;
  padding: 0;
  font-family: ${ props => props.theme.text.highlightText.font };
  font-weight: ${ props => props.theme.text.highlightText.weight };
  font-size: ${ props => props.theme.text.highlightText.size };
  font-style: ${ props => props.theme.text.highlightText.fontStyle };
  display: inline;
  color: ${ props => props.theme.text.highlightText.color.foreground };
  background-color: ${ props => props.theme.text.highlightText.color.background };
`;

HighlightText.defaultProps = { theme: DefaultTheme };

export const BoldText = styled(Text)`
  margin: 0;
  padding: 0;
  font-family: ${ props => props.theme.text.boldText.font };
  font-weight: ${ props => props.theme.text.boldText.weight };
  font-size: ${ props => props.theme.text.boldText.size };
  font-style: ${ props => props.theme.text.boldText.fontStyle };
  display: inline;
  color: ${ props => props.theme.text.boldText.color.foreground };
  background-color: ${ props => props.theme.text.boldText.color.background };
`;

BoldText.defaultProps = { theme: DefaultTheme };
