import React from 'react';

import styled from 'styled-components';


export const OverlayTileMenuContainer = styled.div`
  flex-grow: 1;
  margin: 2em;
  background-color: white;
  overflow-y: scroll;
  z-index: 1;
`;
