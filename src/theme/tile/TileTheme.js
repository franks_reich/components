export const TileTheme = {
  width: '13em',
  height: '13em',
  margin: '1em',
  borderRadius: '1em',
  normal: {
    border: '0.1em solid',
    color: {
      background: 'aliceblue',
      border: 'lightsteelblue'
    }
  },
  hover: {
    border: '0.1em solid',
    shadow: '0.2em 0.2em 1em darkgrey',
    transition: {
      duration: '1s'
    },
    color: {
      background: 'aliceblue',
      border: 'lightsteelblue'
    }
  },
  active: {
    color: {
      background: 'aliceblue',
      border: 'lightsteelblue'
    },
    border: '0.1em solid',
    shadow: '0.2em 0.2em 1em darkgrey inset',
    transition: {
      duration: '0.5s'
    }
  }
};