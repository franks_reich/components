import React from 'react';

import styled from 'styled-components';
import { Link } from 'react-router-dom';

import {
  TileBase, TileTitle, TileSubTitle, TileSubTitleContainer, TileTitleContainer
} from "./TileBase";
import { RowContainer } from "../../basic_layout/Container";

const NavigationTileBase = (props) => {
  const title = props.title;
  const subTitle = props.subTitle;
  const url = props.url;
  return (
    <TileBase>
      <TileTitleContainer>
        <TileTitle>{ title }</TileTitle>
      </TileTitleContainer>
      <TileSubTitleContainer>
        <TileSubTitle>{ subTitle }</TileSubTitle>
      </TileSubTitleContainer>
    </TileBase>
  );
};

const NavigationTileLink = styled(Link)`
  text-decoration: none;
`;

const NavigationTileA = styled.a`
  text-decoration: none;
`;

export const NavigationTile = (props) => {
  const title = props.title;
  const subTitle = props.subTitle;
  const url = props.url;
  const navType = props.navType;
  if (navType === "Router") {
    return (
      <NavigationTileLink to={ url }>
        <NavigationTileBase
          title={ title }
          subTitle={ subTitle }
          url={ url } />
      </NavigationTileLink>
    );
  } else if (navType === "Html") {
    return (
      <NavigationTileA href={ url }>
        <NavigationTileBase
          title={ title }
          subTitle={ subTitle }
          url={ url } />
      </NavigationTileA>
    );
  }
};
