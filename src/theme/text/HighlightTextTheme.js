export const HighlightTextTheme = {
  font: '"Montserrat",sans-serif',
  weight: 'lighter',
  fontStyle: 'italic',
  size: '0.8em',
  color: {
    background: 'transparent',
    foreground: 'black'
  }
};
