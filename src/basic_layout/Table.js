import React from 'react';

import styled from "styled-components";

import { DefaultTheme } from "../theme/DefaultTheme";


export const Table = styled.table`
  flex-grow: 1;
  border: solid thin;
  border-collapse: collapse;
  font-family: ${ props => props.theme.text.normalText.font };
  font-size: ${ props => props.theme.text.normalText.size };
`;

Table.defaultProps = { theme: DefaultTheme };

export const HeaderRow = styled.tr`
  background-color: powderblue;
`;

export const Row = styled.tr`
  :hover {
    background-color: aliceblue;
  }
`;

export const Header = styled.th`
  padding: 1em;
  margin-bottom: 1em;
  border: solid thin;
  font-size: 1em;
  font-weight: bold;
`;

export const Field = styled.td`
  padding: 1em;
  border: solid thin;
  border-bottom: 0;
  border-top: 0;
`;
