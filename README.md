# Frank's Reich Components

Frank's Reich components provides layout, text and basic components for React applications.
Text and basic components are available in several sizes so that the basic components
can be used next to the text components and display the same text size. The sizes are listed
below in descending order from biggest to smallest:
 * ApplicationTitle
 * ComponentTitle
 * ComponentSubTitle
 * ItemTitle
 * ItemSubTitle
 * Text


## Installation

```
npm install franks-reich-components
```


## Theming

The components can be controlled with using a styled-components theme [^1]. To make the components
work out of the box, Frank's Reich Components provides a default theme which is used by all
components unless the theme is overwritten using a ThemeProvider.


## Local Testing

Create link to npm modules directory for the components by calling `npm link` in this directory.
Afterwards call `npm link franks-reich-components` in the react component that wants to use this
component.


# References

[^1]: https://www.styled-components.com/docs/advanced
 
