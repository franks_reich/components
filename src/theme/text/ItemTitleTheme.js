export const ItemTitleTheme = {
  font: '"Montserrat",sans-serif',
  weight: 'light',
  fontStyle: 'bold',
  size: '1.2em',
  color: {
    background: 'transparent',
    foreground: 'black'
  }
};
