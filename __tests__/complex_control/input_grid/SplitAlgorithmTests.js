import React from 'react';

import { splitInputIntoColumns } from "../../../src/complex_control/InputGrid";


describe('The Input Grid split algorithm', () => {
  describe('splitting an empty array', () => {
    describe('into five columns', () => {
      const output = splitInputIntoColumns([], 5);

      it('should return five empty arrays', () => {
        expect(output).toEqual([[], [], [], [], []]);
      });
    });
  });

  describe('splitting an array with four input values', () => {
    const input = [
      {
        label: 'Label 1',
        value: 'Value 1',
        callback: () => {}
      },
      {
        label: 'Label 2',
        value: 'Value 2',
        callback: () => {}
      },
      {
        label: 'Label 3',
        value: 'Value 3',
        callback: () => {}
      },
      {
        label: 'Label 4',
        value: 'Value 4',
        callback: () => {}
      }
    ];

    describe('into 1 column', () => {
      const output = splitInputIntoColumns(input, 1);

      it('should return the input array as only result in the output array', () => {
        expect(output[0]).toEqual(input);
      });
    });

    describe('into 2 columns', () => {
      const output = splitInputIntoColumns(input, 2);

      it('should split the input values into two arrays', () => {
        expect(output.length).toBe(2);
      });

      it('should split the input into arrays of equal size', () => {
        expect(output[0].length).toBe(output[1].length);
      });

      it('should put the first two elements in the first array', () => {
        expect(output[0][0]).toBe(input[0]);
        expect(output[0][1]).toBe(input[1]);
      });

      it('should put the lasts two elements in the second array', () => {
        expect(output[1][0]).toBe(input[2]);
        expect(output[1][1]).toBe(input[3]);
      });
    });

    describe('Into 3 columns', () => {
      const output = splitInputIntoColumns(input, 3);

      it('should return three output arrays', () => {
        expect(output.length).toBe(3);
      });

      it('should put 2 elements in the first output array', () => {
        expect(output[0].length).toBe(2);
      });

      it('should put 1 elements in the following two arrays', () => {
        expect(output[1].length).toBe(1);
        expect(output[2].length).toBe(1);
      });
    });
  });
});
