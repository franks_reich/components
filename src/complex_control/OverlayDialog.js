import React from 'react';

import { OverlayDialogBox } from "./overlay/OverlayDialogBox";
import { ButtonText } from "../basic_control/Button";
import { ItemTitle, Text } from "../basic_layout/Text";
import { RowContainer, Spacer } from "../basic_layout/Container";
import { OverlayBackground } from "./overlay/OverlayBackground";


export const OverlayDialog = (props) => {
  const text = props.text;
  const title = props.title;
  const leftButtonText = props.leftButton.text;
  const leftButtonIconClass = props.leftButton.iconClass;
  const leftButtonOnClick = props.leftButton.onClick;
  const rightButtonText = props.rightButton.text;
  const rightButtonIconClass = props.rightButton.iconClass;
  const rightButtonOnClick = props.rightButton.onClick;
  const backgroundOnClick = props.backgroundOnClick ?
    props.backgroundOnClick : () => {};
  const ignoreEvent = (event) => { event.stopPropagation(); };
  return (
    <OverlayBackground
      onClick={ backgroundOnClick }>
      <OverlayDialogBox
          onClick={ ignoreEvent }>
        <RowContainer>
          <ItemTitle>{ title }</ItemTitle>
        </RowContainer>
        <RowContainer>
          <Text>{ text }</Text>
        </RowContainer>
        <RowContainer>
          <ButtonText
            text={ leftButtonText }
            iconClass={ leftButtonIconClass }
            onClick={ leftButtonOnClick } />
          <Spacer />
          <ButtonText
            text={ rightButtonText }
            iconClass={ rightButtonIconClass }
            onClick={ rightButtonOnClick } />
        </RowContainer>
        <Spacer height="1em" />
      </OverlayDialogBox>
    </OverlayBackground>
  );
};
