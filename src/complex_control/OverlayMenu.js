import React from 'react';

import { curry } from 'ramda';
import styled from "styled-components";

import { RowContainer, Spacer } from "../basic_layout/Container";
import { OverlayPane } from "./OverlayPane";
import { ItemSubTitle } from "../basic_layout/Text";
import { IconItemSubTitle } from "../basic_control/Icon";
import { ButtonBase } from "../basic_control/Button";
import { DefaultTheme } from "../theme/DefaultTheme";
import { HeaderLink } from "./application_header/ApplicationHeaderLink";


const StyledMenuButton = styled.button`
  margin: 0;
  padding: 0;
  background-color: ${ props => props.theme.button.normal.color.background };
  border: none;
  border-color: burlywood;
  transition-duration: ${ props => props.theme.button.normal.transition.duration };
  border-bottom: none;
  width: 100%;
  
  &:hover {
    background-color: rgba(1, 1, 1, 0.1);
    border: none;
    border-color: burlywood;
    box-shadow: 0.1em 0.1em 0.5em darkgrey;
    transition-duration: ${ props => props.theme.button.hover.transition.duration };
    border-bottom: none;
  }
  
  &:active {
    background-color: rgba(1, 1, 1, 0.2);
    border: none;
    box-shadow: ${ props => props.theme.button.active.shadow };
    transition-duration: ${ props => props.theme.button.active.transition.duration };
    border-bottom: none;
  }
  
  &:focus {
    outline: 0;
  }
`;

StyledMenuButton.defaultProps = { theme: DefaultTheme };

const HeaderMenuButton = curry(ButtonBase)(
  StyledMenuButton, IconItemSubTitle, ItemSubTitle);

const PopupMenuDiv = styled.div`
  background-color: floralwhite;
`;

export class OverlayMenu extends React.Component {
  constructor() {
    super();
    this.state = {
      visible: false,
    };

    this.currentDimensions = {
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      width: 0,
      height: 0
    };

    this.contentDivRef = React.createRef();
    this.onClick = this.onClick.bind(this);
    this.measureDiv = this.measureDiv.bind(this);
    this.needsUpdate = this.needsUpdate.bind(this);
    this.callbackAndClose = this.callbackAndClose.bind(this);
  }

  componentDidMount() {
    window.addEventListener("resize", this.needsUpdate);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.needsUpdate);
  }

  needsUpdate() {
    const dimensions = this.measureDiv();
    if (this.currentDimensions.top !== dimensions.top ||
        this.currentDimensions.right !== dimensions.right ||
        this.currentDimensions.bottom !== dimensions.bottom ||
        this.currentDimensions.left !== dimensions.left ||
        this.currentDimensions.width !== dimensions.width ||
        this.currentDimensions.height !== dimensions.height) {
      this.forceUpdate();
    }
  }

  measureDiv() {
    const fixed = this.props.fixed;
    if (this.contentDivRef.current) {
      if (fixed) {
        const boundingRect = this.contentDivRef.current.getBoundingClientRect();
        return {
          top: boundingRect.top - window.scrollY,
          left: boundingRect.left - window.scrollX,
          right: boundingRect.right - window.scrollX,
          bottom: boundingRect.bottom - window.scrollY,
          width: boundingRect.width,
          height: boundingRect.height
        };
      } else {
        const boundingRect = this.contentDivRef.current.getBoundingClientRect();
        return {
          top: boundingRect.top,
          left: boundingRect.left,
          right: boundingRect.right,
          bottom: boundingRect.bottom,
          width: boundingRect.width,
          height: boundingRect.height
        };
      }
    } else {
      return {
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        width: 0,
        height: 0
      };
    }
  }

  render() {
    const Button = this.props.button;
    const text = this.props.text;
    const dimensions = this.measureDiv();
    const overlay = this.createOverlay(dimensions);
    this.currentDimensions = dimensions;
    return (
      <React.Fragment>
        <div ref={ this.contentDivRef }>
          <Button
            onClick={ this.onClick }
            text={ text } />
        </div>
        { overlay }
      </React.Fragment>
    );
  }

  createOverlay(dimensions) {
    const items = this.props.items;
    const content = this.createMenuContent(items);
    if (this.state.visible) {
      const bottom = dimensions.bottom + scrollY;
      return (
        <OverlayPane
          position={{
            top: bottom + "px",
            left: dimensions.left + "px",
            width: dimensions.width + "px"
          }}
          content={ content } />
      );
    } else {
      return null;
    }
  }

  createMenuContent(menuItems) {
    const items = menuItems.map((item, index) =>
      this.createMenuButton(item, index));
    return (
      <PopupMenuDiv>
        { items }
      </PopupMenuDiv>
    );
  }

  createMenuButton(item, index) {
    if (item.type === "Navigation") {
      const onClick = curry(this.callbackAndClose)(() => {});
      return (
        <HeaderLink
          to={ item.url }
          key={ index }>
          <HeaderMenuButton
            onClick={ onClick }
            text={ item.title } />
        </HeaderLink>);
    } else if (item.type === "Button") {
      const onClick = curry(this.callbackAndClose)(item.onClick);
      return (
        <HeaderMenuButton
          onClick={ onClick }
          text={ item.title }
          key={ index } />);
    } else {
      return null;
    }
  }

  callbackAndClose(callback, event) {
    callback(event);
    this.setState({ visible: false });
  }

  onClick() {
    this.setState({
      visible: !this.state.visible
    });
  }
}
