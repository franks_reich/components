import React from 'react';

import { OverlayPaneContainer } from "./overlay/OverlayPaneContainer";


export const OverlayPane = (props) => {
  const content = props.content;
  const position = props.position;
  return (
    <OverlayPaneContainer position={ position } >
      { content }
    </OverlayPaneContainer>
  );
};
