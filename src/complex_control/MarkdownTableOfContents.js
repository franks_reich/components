import React from 'react';

import { HashLink as Link } from 'react-router-hash-link';

import { RowContainer, ColumnContainer } from "../basic_layout/Container";
import { ComponentTitle, HighlightText } from "../basic_layout/Text";


export class MarkdownTableOfContents extends React.Component {
  constructor() {
    super();
  }

  render() {
    const source = this.props.source;
    const linkType = this.props.linkType;
    const baseUrl = this.props.baseUrl;
    const headers = this.parseHeaderInformation(source);
    const links = this.createLinks(headers, linkType, baseUrl);
    return (
      <div>
        <RowContainer />
        <ColumnContainer>
          <ComponentTitle>Table of Contents</ComponentTitle>
          <ColumnContainer>
            { links }
          </ColumnContainer>
        </ColumnContainer>
      </div>
    );
  }

  createLinks(headers, linkType, baseUrl) {
    return headers.map((header, index) =>
      this.createLink(header, linkType, baseUrl, index));
  }

  createLink(header, linkType, baseUrl, key) {
    let linkText = "";
    if (header[2] !== "") {
      linkText = header[2] + ". " + header[3];
    } else {
      linkText = header[3];
    }
    const url = baseUrl + "#" + header[0];
    const innerElement = <HighlightText>{ linkText }</HighlightText>;
    if (linkType === "Router") {
      return (
          <Link
            to={{
              pathname: baseUrl,
              hash: "#" + header[0]
            }}
            key={ key }>
            { innerElement }
          </Link>);
    } else if (linkType === "Html") {
      return <a href={ url } key={ key }>{ innerElement }</a>
    } else {
      return null;
    }
  }

  parseHeaderInformation(source) {
    const results = source.match(/(^<a id=.*\n{0,2}#{1,7}.*)/gm);
    const nameRegExp = /<a id="(.*)"><\/a>/;
    const headingStartRegExp = /^(#*)/;
    const headingWithNumberRegExp = /^#*\s*[0-9.]*.{3}(.*)/;
    const headingRegExp = /^#*\s*(.*)/;
    const headingNumberRegExp = /^#*\s*([0-9.]*)/;
    return results.map(result => {
      const split = result.split('\n');
      const linkName = nameRegExp.exec(split[0])[1];
      const headingLevel = headingStartRegExp.exec(split[2])[1].length;
      const headingNumber = headingNumberRegExp.exec(split[2])[1];
      let heading = "";
      if (headingNumber !== "") {
        heading = headingWithNumberRegExp.exec(split[2])[1];
      } else {
        heading = headingRegExp.exec(split[2])[1];
      }
      return [linkName, headingLevel, headingNumber, heading];
    });
  }
}
