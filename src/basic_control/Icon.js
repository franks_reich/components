import React from 'react';

import styled from "styled-components";

import { DefaultTheme } from "../theme/DefaultTheme";


export const IconApplicationTitle = styled.i`
  margin: auto;
  font-size: ${ props => props.theme.text.applicationTitle.size };
`;

IconApplicationTitle.defaultProps = { theme: DefaultTheme };

export const IconComponentTitle = styled.i`
  margin: auto;
  font-size: ${ props => props.theme.text.componentTitle.size };
`;

IconComponentTitle.defaultProps = { theme: DefaultTheme };

export const IconComponentSubTitle = styled.i`
  margin: auto;
  font-size: ${ props => props.theme.text.componentSubTitle.size };
`;

IconComponentSubTitle.defaultProps = { theme: DefaultTheme };

export const IconItemTitle = styled.i`
  margin: auto;
  font-size: ${ props => props.theme.text.itemTitle.size };
`;

IconItemTitle.defaultProps = { theme: DefaultTheme };

export const IconItemSubTitle = styled.i`
  margin: auto;
  font-size: ${ props => props.theme.text.itemSubTitle.size };
`;

IconItemSubTitle.defaultProps = { theme: DefaultTheme };

export const IconText = styled.i`
  margin: auto;
  font-size: ${ props => props.theme.text.normalText.size };
`;

IconText.defaultProps = { theme: DefaultTheme };
