import React from 'react';

import styled from 'styled-components';

import { RowContainer, Spacer, ColumnContainer } from "../../basic_layout/Container";


const PanelContainer = styled.div`
  box-shadow: 0.1em 0.1em 0.5em darkgrey;
  width: 100%;
  display: flex;
  flex-direction: column;
`;

const InnerContainer = styled(ColumnContainer)`
  padding: 0;
`;

export class InnerPanel extends React.Component {
  constructor() {
    super();

    this.state = { open: true };

    this.onClick = this.onClick.bind(this);
  }

  render() {
    const Title = this.props.Title;
    const title = this.props.title;
    const content = this.state.open ?
      <InnerContainer>
        { this.props.content }
      </InnerContainer> : null;
    const icon = this.createIcon();
    return (
      <PanelContainer>
        <RowContainer
          onClick={ this.onClick }>
          <Title>{ title }</Title>
          <Spacer />
          { icon }
        </RowContainer>
        { content }
        <Spacer height="1em" />
      </PanelContainer>
    )
  }

  onClick() {
    this.setState({ open: !this.state.open });
  }

  createIcon() {
    const Icon = this.props.Icon;
    if (this.state.open) {
      return (<Icon className="fas fa-caret-up" />);
    } else {
      return (<Icon className="fas fa-caret-down" />);
    }
  }
}
