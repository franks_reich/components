import { ApplicationTitleTheme } from "./ApplicationTitleTheme";
import { ApplicationHighlightTitleTheme } from "./ApplicationHighlightTitleTheme";
import { ComponentTitleTheme } from "./ComponentTitleTheme";
import { ComponentHighlightTitleTheme } from "./ComponentHighlightTitleTheme";
import { ComponentSubTitleTheme } from "./ComponentSubTitleTheme";
import { ComponentHighlightSubTitleTheme } from "./ComponentHighlightSubTitleTheme";
import { ItemTitleTheme } from "./ItemTitleTheme";
import { ItemHighlightTitleTheme } from "./ItemHighlightTitleTheme";
import { ItemSubTitleTheme } from "./ItemSubTitleTheme";
import { ItemHighlightSubTitleTheme } from "./ItemHighlightSubTitleTheme";
import { NormalTextTheme } from "./NormalTextTheme";
import { HighlightTextTheme } from "./HighlightTextTheme";
import { BoldTextTheme } from "./BoldTextTheme";


export const TextTheme = {
  applicationTitle: ApplicationTitleTheme,
  applicationHighlightTitle: ApplicationHighlightTitleTheme,
  componentTitle: ComponentTitleTheme,
  componentHighlightTitle: ComponentHighlightTitleTheme,
  componentSubTitle: ComponentSubTitleTheme,
  componentHighlightSubTitle: ComponentHighlightSubTitleTheme,
  itemTitle: ItemTitleTheme,
  itemHighlightTitle: ItemHighlightTitleTheme,
  itemSubTitle: ItemSubTitleTheme,
  itemHighlightSubTitle: ItemHighlightSubTitleTheme,
  normalText: NormalTextTheme,
  highlightText: HighlightTextTheme,
  boldText: BoldTextTheme
};
