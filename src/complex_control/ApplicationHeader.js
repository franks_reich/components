import React from 'react';

import Measure from 'react-measure';

import { ApplicationHeaderContainer } from "./application_header/ApplicationHeaderContainer";
import { PaddingContainer, Spacer, CenterRowContainer } from "../basic_layout/Container";
import { ApplicationTitle } from "../basic_layout/Text";
import { ApplicationHeaderMenu } from "./application_header/ApplicationHeaderMenu";
import { HeaderLink } from "./application_header/ApplicationHeaderLink";


export class ApplicationHeader extends React.Component {
  constructor() {
    super();
    this.state = {
      dimensions: {
        width: 0,
        height: 0
      }
    }
  }

  render() {
    let content = null;
    const menu = this.props.menu;
    const title = this.props.title;
    const homeUrl = this.props.homeUrl;
    const linkType = this.props.navType;
    if (this.props.fixed) {
      content = this.createFixedApplicationHeader(
        menu, title, homeUrl, linkType)
    } else {
      content = this.createApplicationHeader(
        menu, title, homeUrl, linkType);
    }
    return content;
  }

  createFixedApplicationHeader(menu, title, homeUrl, linkType) {
    const applicationHeader = this.createApplicationHeader(
      menu, title, homeUrl, linkType);
    return (
      <React.Fragment>
        <Measure
          bounds
          onResize={ (rectangle) => {
            this.setState({ dimensions: rectangle.bounds })
          }}>
          {({ measureRef }) =>
            <div ref={ measureRef } style={{
              position: "fixed",
              width: "100%",
              top: 0
            }}>
              { applicationHeader }
            </div>
          }
        </Measure>
        <Spacer height={ this.state.dimensions.height + 'px' } />
      </React.Fragment>
    );
  }

  createApplicationHeader(menu, title, homeUrl, linkType) {
    const applicationHeaderContent = this.createApplicationHeaderContent(
      menu, title, homeUrl, linkType);
    return (
      <ApplicationHeaderContainer>
        {applicationHeaderContent}
      </ApplicationHeaderContainer>
    );
  }

  createApplicationHeaderContent(menu, title, homeUrl, linkType) {
    const titleElement = this.createApplicationTitle(title);
    if (linkType === "Router") {
      if (menu) {
        return (
          <React.Fragment>
            <HeaderLink to={ homeUrl }>
              <CenterRowContainer>
                { titleElement }
              </CenterRowContainer>
            </HeaderLink>
            <ApplicationHeaderMenu menu={ menu } />
          </React.Fragment>
        );
      } else {
        return (
          <HeaderLink to={ homeUrl }>
            <PaddingContainer>
              { titleElement }
            </PaddingContainer>
          </HeaderLink>
        );
      }
    } else if (linkType === "Html") {
      if (menu) {
        return (
          <React.Fragment>
            <a href={ homeUrl }>
              <CenterRowContainer>
                { titleElement }
              </CenterRowContainer>
            </a>
            <ApplicationHeaderMenu menu={ menu } />
          </React.Fragment>
        );
      } else {
        return (
          <a href={ homeUrl }>
            <PaddingContainer>
              { titleElement }
            </PaddingContainer>
          </a>
        );
      }
    } else {
      if (menu) {
        return (
          <React.Fragment>
            <CenterRowContainer>
              { titleElement }
            </CenterRowContainer>
            <ApplicationHeaderMenu menu={ menu } />
          </React.Fragment>
        );
      } else {
        return (
          <PaddingContainer>
            { titleElement }
          </PaddingContainer>
        );
      }
    }
  }

  createApplicationTitle(title) {
    return (
      <ApplicationTitle>
        {title}
      </ApplicationTitle>
    );
  }
}
