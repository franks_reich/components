export const ItemHighlightTitleTheme = {
  font: '"Montserrat",sans-serif',
  weight: 'bold',
  fontStyle: 'normal',
  size: '1.2em',
  color: {
    background: 'transparent',
    foreground: 'blue'
  }
};
