import { TextTheme } from "./text/TextTheme";
import { ButtonTheme } from "./button/ButtonTheme";
import { TileTheme } from "./tile/TileTheme";
import { OverlayBackgroundTheme } from "./overlay/OverlayBackgroundTheme";
import { OverlayIcon } from "./overlay/OverlayIcon";


export const DefaultTheme = {
  text: TextTheme,
  tile: TileTheme,
  button: ButtonTheme,
  overlayBackground: OverlayBackgroundTheme,
  overlayIcon: OverlayIcon
};
