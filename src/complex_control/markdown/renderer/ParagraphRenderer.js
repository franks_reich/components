import React from 'react';

import { Text } from '../../../basic_layout/Text';
import { ContentContainer } from "../MarkdownContainer";


export const ParagraphRenderer = (props) => {
  const children = props.children;
  return (
    <ContentContainer>
      <Text>{ children }</Text>
    </ContentContainer>
  );
};
