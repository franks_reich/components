import React from 'react';

import styled from "styled-components";

import { RowContainer } from "../../basic_layout/Container";


export const MarkdownContainer = styled(RowContainer)`
  padding: 0;
  display: block;
  flex-grow: 1;
`;

export const FirstLevelContainer = styled(RowContainer)`
  padding-left: 1.0em;
  display: block;
`;

export const SecondLevelContainer = styled(RowContainer)`
  padding-left: 1.1em;
  display: block;
`;

export const ThirdLevelContainer = styled(RowContainer)`
  padding-left: 1.2em;
  display: block;
`;

export const FourthLevelContainer = styled(RowContainer)`
  padding-left: 1.3em;
  display: block;
`;

export const ContentContainer = styled(RowContainer)`
  padding-left: 1.5em;
  display: block;
`;
