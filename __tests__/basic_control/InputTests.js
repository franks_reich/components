import React from 'react';
import { shallow } from 'enzyme';
import 'jest-styled-components'

import {
  ApplicationTitle, ApplicationTitleInput,
  ComponentTitleInput, ComponentTitle, ComponentSubTitleInput, ComponentSubTitle,
  ItemTitle, ItemTitleInput, ItemSubTitle, ItemSubTitleInput, Text, TextInput
} from "../../src";

import {
  InputRow, ComponentTitleInputTag, ComponentSubTitleInputTag, ItemTitleInputTag,
  ItemSubTitleInputTag, TextInputTag, ApplicationTitleInputTag
} from "../../src/basic_control/Input";

import { DefaultTheme } from "../../src";


const inputRowTest = (inputRow) => {
  expect(inputRow).toHaveStyleRule('display', 'flex');
  expect(inputRow).toHaveStyleRule('flex-direction', 'row');
  expect(inputRow).toHaveStyleRule('margin', '0');
  expect(inputRow).toHaveStyleRule('padding', '0');
  expect(inputRow).toHaveStyleRule('flex-grow', '1');
};

const inputTest = (input, fontSize) => {
  expect(input).toHaveStyleRule('margin', '0 0 0 0.2em');
  expect(input).toHaveStyleRule('font-family', DefaultTheme.text.font);
  expect(input).toHaveStyleRule('font-weight', DefaultTheme.text.weight);
  expect(input).toHaveStyleRule('font-size', fontSize);
  expect(input).toHaveStyleRule('border', 'none');
  expect(input).toHaveStyleRule('display', 'inline');
  expect(input).toHaveStyleRule('background-color', DefaultTheme.text.color.background);
};

const labelTest = (label, fontSize) => {
  expect(label).toHaveStyleRule('margin', '0');
  expect(label).toHaveStyleRule('padding', '0');
  expect(label).toHaveStyleRule('font-family', DefaultTheme.text.font);
  expect(label).toHaveStyleRule('font-weight', DefaultTheme.text.weight);
  expect(label).toHaveStyleRule('font-size', fontSize);
  expect(label).toHaveStyleRule('display', 'inline');
  expect(label).toHaveStyleRule('color', DefaultTheme.text.color.foreground);
  expect(label).toHaveStyleRule('background-color', DefaultTheme.text.color.background);
};

test(
  'ApplicationTitleInput should be a div containing an ApplicationTitle and an input. ' +
  'They should have the default properties', () => {
    const onChange = jest.fn();
    const applicationTitleInput = shallow(
      <ApplicationTitleInput
        lable='Test'
        value='Initial'
        onChange={ onChange } />);
    const inputRow = applicationTitleInput.find(InputRow);
    inputRowTest(inputRow);
    const input = applicationTitleInput.find(ApplicationTitleInputTag);
    inputTest(input, DefaultTheme.text.size.applicationTitle);
    const label = applicationTitleInput.find(ApplicationTitle);
    labelTest(label, DefaultTheme.text.size.applicationTitle);
    input.simulate('change');
    expect(onChange).toHaveBeenCalled();
  });

test(
  'ComponentTitleInput should be a div containing a ComponentTitle and an input. ' +
  'They should have the defined properties', () => {
    const onChange = jest.fn();
    const componentTitleInput = shallow(
      <ComponentTitleInput
        lable='Test'
        value='Initial'
        onChange={ onChange } />);
    const inputRow = componentTitleInput.find(InputRow);
    inputRowTest(inputRow);
    const input = componentTitleInput.find(ComponentTitleInputTag);
    inputTest(input, DefaultTheme.text.size.componentTitle);
    const label = componentTitleInput.find(ComponentTitle);
    labelTest(label, DefaultTheme.text.size.componentTitle);
    input.simulate('change');
    expect(onChange).toHaveBeenCalled();
  });

test(
  'ComponentSubTitleInput should be a div containing a ComponentSubTitle and an input. ' +
  'They should have the defined properties', () => {
    const onChange = jest.fn();
    const componentTitleInput = shallow(
      <ComponentSubTitleInput
        lable='Test'
        value='Initial'
        onChange={ onChange } />);
    const inputRow = componentTitleInput.find(InputRow);
    inputRowTest(inputRow);
    const input = componentTitleInput.find(ComponentSubTitleInputTag);
    inputTest(input, DefaultTheme.text.size.componentSubTitle);
    const label = componentTitleInput.find(ComponentSubTitle);
    labelTest(label, DefaultTheme.text.size.componentSubTitle);
    input.simulate('change');
    expect(onChange).toHaveBeenCalled();
  });

test(
  'ItemTitleInput should be a div containing a ItemTitle and an input. ' +
  'They should have the defined properties', () => {
    const onChange = jest.fn();
    const componentTitleInput = shallow(
      <ItemTitleInput
        lable='Test'
        value='Initial'
        onChange={ onChange } />);
    const inputRow = componentTitleInput.find(InputRow);
    inputRowTest(inputRow);
    const input = componentTitleInput.find(ItemTitleInputTag);
    inputTest(input, DefaultTheme.text.size.itemTitle);
    const label = componentTitleInput.find(ItemTitle);
    labelTest(label, DefaultTheme.text.size.itemTitle);
    input.simulate('change');
    expect(onChange).toHaveBeenCalled();
  });

test(
  'ItemSubTitleInput should be a div containing a ItemSubTitle and an input. ' +
  'They should have the defined properties', () => {
    const onChange = jest.fn();
    const componentTitleInput = shallow(
      <ItemSubTitleInput
        lable='Test'
        value='Initial'
        onChange={ onChange } />);
    const inputRow = componentTitleInput.find(InputRow);
    inputRowTest(inputRow);
    const input = componentTitleInput.find(ItemSubTitleInputTag);
    inputTest(input, DefaultTheme.text.size.itemSubTitle);
    const label = componentTitleInput.find(ItemSubTitle);
    labelTest(label, DefaultTheme.text.size.itemSubTitle);
    input.simulate('change');
    expect(onChange).toHaveBeenCalled();
  });

test(
  'TextInput should be a div containing a Text and an input. ' +
  'They should have the defined properties', () => {
    const onChange = jest.fn();
    const componentTitleInput = shallow(
      <TextInput
        lable='Test'
        value='Initial'
        onChange={ onChange } />);
    const inputRow = componentTitleInput.find(InputRow);
    inputRowTest(inputRow);
    const input = componentTitleInput.find(TextInputTag);
    inputTest(input, DefaultTheme.text.size.text);
    const label = componentTitleInput.find(Text);
    labelTest(label, DefaultTheme.text.size.text);
    input.simulate('change');
    expect(onChange).toHaveBeenCalled();
  });
