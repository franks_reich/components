import React from 'react';

import styled from 'styled-components';
import { curry } from 'ramda';

import { DefaultTheme } from "../theme/DefaultTheme";

import {
  ApplicationTitle, ComponentTitle, ComponentSubTitle, ItemTitle, ItemSubTitle, Text
} from "../basic_layout/Text";

import { Spacer } from '../basic_layout/Container'

import {
  IconApplicationTitle, IconComponentTitle, IconComponentSubTitle,
  IconItemTitle, IconItemSubTitle, IconText
} from "./Icon";


export const InputRow = styled.div`
  display: flex;
  flex-direction: row;
  margin: 0;
  padding: 0;
  flex-grow: 1;
`;

const InputBase = (Label, Input, Icon, props) => {
  const labelText = props.label;
  const value = props.value;
  const onChange = props.onChange ? props.onChange : () => {};
  return (
    <InputRow>
      <Label>{ labelText }</Label>
      <Spacer width='1em' />
      <Icon className='fas fa-angle-double-right' />
      <Spacer width='0.5em' />
      <Input
        value={ value }
        onChange={ onChange } />
    </InputRow>
  );
};

export const ApplicationTitleInputTag = styled.input`
  padding: 0.2em;
  margin: 0;
  font-weight: bold;
  font-family: ${ props => props.theme.text.applicationTitle.font };
  font-size: ${ props => props.theme.text.applicationTitle.size };
  border: none;
  border-bottom: solid thin;
  display: inline;
  background-color: ${ props => props.theme.text.applicationTitle.color.background };
  color: ${ props => props.theme.text.applicationTitle.color.foreground };
  flex-grow: 1;
  
  :hover {
    box-shadow: 0.2em 0.2em 1em darkgrey;
    border: solid thin;
  }
  
  :focus {
    box-shadow: 0.2em 0.2em 1em darkgrey;
    border: solid thin;
  }
`;

ApplicationTitleInputTag.defaultProps = { theme: DefaultTheme };


export const ComponentTitleInputTag = styled.input`
  padding: 0.2em;
  margin: 0em;
  font-weight: bold;
  font-family: ${ props => props.theme.text.componentTitle.font };
  font-size: ${ props => props.theme.text.componentTitle.size };
  border: none;
  border-bottom: solid thin;
  display: inline;
  background-color: ${ props => props.theme.text.componentTitle.color.background };
  color: ${ props => props.theme.text.componentTitle.color.foreground };
  flex-grow: 1;
  
  :hover {
    box-shadow: 0.2em 0.2em 1em darkgrey;
    border: solid thin;
  }
  
  :focus {
    box-shadow: 0.2em 0.2em 1em darkgrey;
    border: solid thin;
  }
`;

ComponentTitleInputTag.defaultProps = { theme: DefaultTheme };


export const ComponentSubTitleInputTag = styled.input`
  padding: 0.2em;
  margin: 0em;
  font-weight: bold;
  font-family: ${ props => props.theme.text.componentSubTitle.font };
  font-size: ${ props => props.theme.text.componentSubTitle.size };
  border: none;
  border-bottom: solid thin;
  display: inline;
  background-color: ${ props => props.theme.text.componentSubTitle.color.background };
  color: ${ props => props.theme.text.componentSubTitle.color.foreground };
  flex-grow: 1;
  
  :hover {
    box-shadow: 0.2em 0.2em 1em darkgrey;
    border: solid thin;
  }
  
  :focus {
    box-shadow: 0.2em 0.2em 1em darkgrey;
    border: solid thin;
  }
`;

ComponentSubTitleInputTag.defaultProps = { theme: DefaultTheme };


export const ItemTitleInputTag = styled.input`
  padding: 0.2em;
  margin: 0em;
  font-weight: bold;
  font-family: ${ props => props.theme.text.itemTitle.font };
  font-size: ${ props => props.theme.text.itemTitle.size };
  border: none;
  border-bottom: solid thin;
  display: inline;
  background-color: ${ props => props.theme.text.itemTitle.color.background };
  color: ${ props => props.theme.text.itemTitle.color.foreground };
  flex-grow: 1;
  
  :hover {
    box-shadow: 0.2em 0.2em 1em darkgrey;
    border: solid thin;
  }
  
  :focus {
    box-shadow: 0.2em 0.2em 1em darkgrey;
    border: solid thin;
  }
`;

ItemTitleInputTag.defaultProps = { theme: DefaultTheme };


export const ItemSubTitleInputTag = styled.input`
  padding: 0.2em;
  margin: 0em;
  font-weight: bold;
  font-family: ${ props => props.theme.text.itemSubTitle.font };
  font-size: ${ props => props.theme.text.itemSubTitle.size };
  border: none;
  border-bottom: solid thin;
  display: inline;
  background-color: ${ props => props.theme.text.itemSubTitle.color.background };
  color: ${ props => props.theme.text.itemSubTitle.color.foreground };
  flex-grow: 1;
  
  :hover {
    box-shadow: 0.2em 0.2em 1em darkgrey;
    border: solid thin;
  }
  
  :focus {
    box-shadow: 0.2em 0.2em 1em darkgrey;
    border: solid thin;
  }
`;

ItemSubTitleInputTag.defaultProps = { theme: DefaultTheme };


export const TextInputTag = styled.input`
  padding: 0.2em;
  margin: 0em;
  font-weight: bold;
  font-family: ${ props => props.theme.text.normalText.font };
  font-size: ${ props => props.theme.text.normalText.size.text };
  border: none;
  border-bottom: solid thin;
  display: inline;
  background-color: ${ props => props.theme.text.normalText.color.background };
  color: ${ props => props.theme.text.normalText.color.foreground };
  flex-grow: 1;
  
  :hover {
    box-shadow: 0.2em 0.2em 1em darkgrey;
    border: solid thin;
  }
  
  :focus {
    box-shadow: 0.2em 0.2em 1em darkgrey;
    border: solid thin;
  }
`;

TextInputTag.defaultProps = { theme: DefaultTheme };

export const TextInputText = styled(Text)`
  align-self: center;
`;

export const ApplicationTitleInput = curry(InputBase)(
  ApplicationTitle, ApplicationTitleInputTag, IconApplicationTitle);
export const ComponentTitleInput = curry(InputBase)(
  ComponentTitle, ComponentTitleInputTag, IconComponentTitle);
export const ComponentSubTitleInput = curry(InputBase)(
  ComponentSubTitle, ComponentSubTitleInputTag, IconComponentSubTitle);
export const ItemTitleInput = curry(InputBase)(
  ItemTitle, ItemTitleInputTag, IconItemTitle);
export const ItemSubTitleInput = curry(InputBase)(
  ItemSubTitle, ItemSubTitleInputTag, IconItemSubTitle);
export const TextInput = curry(InputBase)(
  TextInputText, TextInputTag, IconText);
