export const ButtonTheme = {
  normal: {
    color: {
      background: 'floralwhite',
      border: 'black'
    },
    border: 'solid thin',
    shadow: 'none',
    transition: {
      duration: '2s'
    }
  },
  hover: {
    color: {
      background: 'lightgrey',
      border: 'red'
    },
    border: 'solid thin',
    shadow: '0.2em 0.2em 1em darkgrey',
    transition: {
      duration: '1s'
    }
  },
  active: {
    color: {
      background: 'lightgrey',
      border: 'red'
    },
    border: 'solid thin',
    shadow: '0.2em 0.2em 1em darkgrey inset',
    transition: {
      duration: '0.5s'
    }
  }
};
