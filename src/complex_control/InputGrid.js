import React from 'react';

import styled from 'styled-components';
import { curry } from 'ramda';

import { ColumnContainer, RowContainer, PaddingContainer } from "../basic_layout/Container";
import { Text } from "../basic_layout/Text";
import { TextInput } from "../basic_control/Input";
import {
  TextInputTag
} from "../basic_control/Input";


export const splitInputIntoColumns = (data, columnCount) => {
  const fixedItemsPerColumn = Math.floor(data.length / columnCount);
  const itemsPerColumn = Array(columnCount).fill(fixedItemsPerColumn);
  const remainder = data.length - (fixedItemsPerColumn * columnCount);
  let index = 0;
  for (let i = 0; i < remainder; i++) {
    itemsPerColumn[index] += 1;
    index = (index + 1) % columnCount;
  }
  let columns = Array(columnCount);
  for (let i = 0; i < columnCount; i++) {
    let begin = 0;
    if (i > 0) {
      begin = itemsPerColumn.slice(0, i).reduce((x, y) => x + y);
    }
    const end = begin + itemsPerColumn[i];
    columns[i] = data.slice(begin, end);
  }
  return columns;
};

const Grid = styled.div`
  display: grid;
  grid-template-columns: ${ props => props.templateColumns };
  grid-template-rows: ${ props => props.templateRows };
`;

const layoutInGridOrder = (columns, columnCount, rowCount) => {
  let result = [];
  for (let row = 0; row < rowCount; row++) {
    for (let column = 0; column < columnCount; column++) {
      if (columns[column].length > row) {
        result.push(columns[column][row]);
      }
    }
  }
  return result;
};

const createInputGrid = (Text, Input, data, columnCount) => {
  const columns = splitInputIntoColumns(data, columnCount);
  const rowCount = columns.map(column => column.length)
    .reduce((x, y) => Math.max(x, y));
  const templateColumns = Array(2 * columnCount).fill("1fr").join(' ');
  const templateRows = Array(rowCount).fill("1fr").join(' ');
  const itemsInGridOrder = layoutInGridOrder(columns, columnCount, rowCount);
  let index = 0;
  const gridElements = itemsInGridOrder.map(item => {
    const textIndex = index;
    const inputIndex = index + 1;
    index += 2;
    return (
      [
        <PaddingContainer key={ textIndex }>
          <Text>{ item.label }</Text>
        </PaddingContainer>,
        <PaddingContainer key={ inputIndex }>
          <Input
            value={ item.value }
            onChange={ item.onChange } />
        </PaddingContainer>
      ]
    );
  }).flat();
  return (
    <Grid
      templateColumns={ templateColumns }
      templateRows={ templateRows }>
      { gridElements }
    </Grid>
  );
};

export const InputGrid = (Text, Input, props) => {
  const GridText = styled(Text)`
    margin: auto;
  `;
  const data = props.data;
  const columnCount = props.columnCount;
  const grid = createInputGrid(GridText, Input, data, columnCount);
  return (
    <RowContainer>
      { grid }
    </RowContainer>
  );
};

export const TextInputGrid = curry(InputGrid)(Text, TextInputTag);
