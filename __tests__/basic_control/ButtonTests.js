import React from 'react';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';
import 'jest-styled-components'

import {
  ButtonApplicationTitle, ButtonComponentTitle, ButtonComponentSubTitle, ButtonItemSubTitle,
  ButtonItemTitle, ButtonText
} from '../../src/';

import {
  IconApplicationTitle, IconComponentTitle, IconComponentSubTitle, IconItemSubTitle,
  IconItemTitle, IconText
} from '../../src';

import {
  ApplicationTitle, ComponentTitle, ComponentSubTitle, ItemTitle, ItemSubTitle, Text
} from '../../src';


import { DefaultTheme, Spacer } from '../../src/';
import { Button } from '../../src/basic_control/Button'

describe('The ButtonApplicationTitle', () => {
  it('should have only an IconApplicationTitle if only an icon class is supplied', () => {
    const testRenderer = renderer.create(<ButtonApplicationTitle iconClass='test' />);
    testRenderer.root.findByType(IconApplicationTitle);
    expect(() => testRenderer.root.findByType(ApplicationTitle)).toThrow();
  });

  it('should have only an ApplicationTitle if only a text is supplied', () => {
    const testRenderer = renderer.create(<ButtonApplicationTitle text='test' />);
    testRenderer.root.findByType(ApplicationTitle);
    expect(() => testRenderer.root.findByType(IconApplicationTitle)).toThrow();
  });

  it(
    'should have an ApplicationTitle, an IconApplicationTitle and a Spacer if an icon ' +
    'class and a text is supplied', () => {
      const testRenderer = renderer.create(
        <ButtonApplicationTitle
          text='test'
          iconClass='test' />);
      testRenderer.root.findByType(ApplicationTitle);
      testRenderer.root.findByType(IconApplicationTitle);
      testRenderer.root.findByType(Spacer);
    });

  it('should call the supplied event handler if clicked', () => {
    const onClickMock = jest.fn();
    const button = shallow(<ButtonApplicationTitle onClick={ onClickMock }/>);
    button.simulate('click');
    expect(onClickMock).toHaveBeenCalled();
  });
});

describe('The ButtonComponentTitle', () => {
  it('should have only an IconComponentTitle if only an icon class is supplied', () => {
    const testRenderer = renderer.create(<ButtonComponentTitle iconClass='test' />);
    testRenderer.root.findByType(IconComponentTitle);
    expect(() => testRenderer.root.findByType(ComponentTitle)).toThrow();
  });

  it('should have only an ComponentTitle if only a text is supplied', () => {
    const testRenderer = renderer.create(<ButtonComponentTitle text='test' />);
    testRenderer.root.findByType(ComponentTitle);
    expect(() => testRenderer.root.findByType(IconComponentTitle)).toThrow();
  });

  it(
    'should have an ComponentTitle, an IconComponentTitle and a Spacer if an icon ' +
    'class and a text is supplied', () => {
      const testRenderer = renderer.create(
        <ButtonComponentTitle
          text='test'
          iconClass='test' />);
      testRenderer.root.findByType(ComponentTitle);
      testRenderer.root.findByType(IconComponentTitle);
      testRenderer.root.findByType(Spacer);
    });

  it('should call the supplied event handler if clicked', () => {
    const onClickMock = jest.fn();
    const button = shallow(<ButtonComponentTitle onClick={ onClickMock }/>);
    button.simulate('click');
    expect(onClickMock).toHaveBeenCalled();
  });
});

describe('The ButtonComponentSubTitle', () => {
  it('should have only an IconComponentSubTitle if only an icon class is supplied', () => {
    const testRenderer = renderer.create(<ButtonComponentSubTitle iconClass='test' />);
    testRenderer.root.findByType(IconComponentSubTitle);
    expect(() => testRenderer.root.findByType(ComponentSubTitle)).toThrow();
  });

  it('should have only an ComponentSubTitle if only a text is supplied', () => {
    const testRenderer = renderer.create(<ButtonComponentSubTitle text='test' />);
    testRenderer.root.findByType(ComponentSubTitle);
    expect(() => testRenderer.root.findByType(IconComponentSubTitle)).toThrow();
  });

  it(
    'should have an ComponentSubTitle, an IconComponentSubTitle and a Spacer if an icon ' +
    'class and a text is supplied', () => {
      const testRenderer = renderer.create(
        <ButtonComponentSubTitle
          text='test'
          iconClass='test' />);
      testRenderer.root.findByType(ComponentSubTitle);
      testRenderer.root.findByType(IconComponentSubTitle);
      testRenderer.root.findByType(Spacer);
    });

  it('should call the supplied event handler if clicked', () => {
    const onClickMock = jest.fn();
    const button = shallow(<ButtonComponentSubTitle onClick={ onClickMock }/>);
    button.simulate('click');
    expect(onClickMock).toHaveBeenCalled();
  });
});

describe('The ButtonItemTitle', () => {
  it('should have only an IconItemTitle if only an icon class is supplied', () => {
    const testRenderer = renderer.create(<ButtonItemTitle iconClass='test' />);
    testRenderer.root.findByType(IconItemTitle);
    expect(() => testRenderer.root.findByType(ItemTitle)).toThrow();
  });

  it('should have only an ItemTitle if only a text is supplied', () => {
    const testRenderer = renderer.create(<ButtonItemTitle text='test' />);
    testRenderer.root.findByType(ItemTitle);
    expect(() => testRenderer.root.findByType(IconItemTitle)).toThrow();
  });

  it(
    'should have an ItemTitle, an IconItemTitle and a Spacer if an icon ' +
    'class and a text is supplied', () => {
      const testRenderer = renderer.create(
        <ButtonItemTitle
          text='test'
          iconClass='test' />);
      testRenderer.root.findByType(ItemTitle);
      testRenderer.root.findByType(IconItemTitle);
      testRenderer.root.findByType(Spacer);
    });

  it('should call the supplied event handler if clicked', () => {
    const onClickMock = jest.fn();
    const button = shallow(<ButtonItemTitle onClick={ onClickMock }/>);
    button.simulate('click');
    expect(onClickMock).toHaveBeenCalled();
  });
});

describe('The ButtonItemSubTitle', () => {
  it('should have only an IconItemSubTitle if only an icon class is supplied', () => {
    const testRenderer = renderer.create(<ButtonItemSubTitle iconClass='test' />);
    testRenderer.root.findByType(IconItemSubTitle);
    expect(() => testRenderer.root.findByType(ItemSubTitle)).toThrow();
  });

  it('should have only an ItemSubTitle if only a text is supplied', () => {
    const testRenderer = renderer.create(<ButtonItemSubTitle text='test' />);
    testRenderer.root.findByType(ItemSubTitle);
    expect(() => testRenderer.root.findByType(IconItemSubTitle)).toThrow();
  });

  it(
    'should have an ItemSubTitle, an IconItemSubTitle and a Spacer if an icon ' +
    'class and a text is supplied', () => {
      const testRenderer = renderer.create(
        <ButtonItemSubTitle
          text='test'
          iconClass='test' />);
      testRenderer.root.findByType(ItemSubTitle);
      testRenderer.root.findByType(IconItemSubTitle);
      testRenderer.root.findByType(Spacer);
    });

  it('should call the supplied event handler if clicked', () => {
    const onClickMock = jest.fn();
    const button = shallow(<ButtonItemSubTitle onClick={ onClickMock }/>);
    button.simulate('click');
    expect(onClickMock).toHaveBeenCalled();
  });
});

describe('The ButtonText', () => {
  it('should have only an IconText if only an icon class is supplied', () => {
    const testRenderer = renderer.create(<ButtonText iconClass='test' />);
    testRenderer.root.findByType(IconText);
    expect(() => testRenderer.root.findByType(Text)).toThrow();
  });

  it('should have only a Text if only a text is supplied', () => {
    const testRenderer = renderer.create(<ButtonText text='test' />);
    testRenderer.root.findByType(Text);
    expect(() => testRenderer.root.findByType(IconText)).toThrow();
  });

  it(
    'should have an Text, an IconText and a Spacer if an icon ' +
    'class and a text is supplied', () => {
      const testRenderer = renderer.create(
        <ButtonText
          text='test'
          iconClass='test' />);
      testRenderer.root.findByType(Text);
      testRenderer.root.findByType(IconText);
      testRenderer.root.findByType(Spacer);
    });

  it('should call the supplied event handler if clicked', () => {
    const onClickMock = jest.fn();
    const button = shallow(<ButtonText onClick={ onClickMock }/>);
    button.simulate('click');
    expect(onClickMock).toHaveBeenCalled();
  });
});

describe('The style of the base for all buttons', () => {
  it('should have a margin of 0', () => {
    const testRenderer = renderer.create(<Button/>);
    const tree = testRenderer.toJSON();
    expect(tree).toHaveStyleRule('margin', '0');
  });

  it('should have a padding of 0', () => {
    const testRenderer = renderer.create(<Button/>);
    const tree = testRenderer.toJSON();
    expect(tree).toHaveStyleRule('padding', '0');
  });

  describe('in normal state', () => {
    it('should have the normal background color', () => {
      const testRenderer = renderer.create(<Button/>);
      const tree = testRenderer.toJSON();
      expect(tree).toHaveStyleRule(
        'background-color',
        DefaultTheme.button.normal.color.background);
    });

    it('should have the normal border color', () => {
      const testRenderer = renderer.create(<Button/>);
      const tree = testRenderer.toJSON();
      expect(tree).toHaveStyleRule(
        'border-color',
        DefaultTheme.button.normal.color.border);
    });

    it('should have the normal border', () => {
      const testRenderer = renderer.create(<Button/>);
      const tree = testRenderer.toJSON();
      expect(tree).toHaveStyleRule(
        'border',
        DefaultTheme.button.normal.border);
    });

    it('should have the normal box shadow', () => {
      const testRenderer = renderer.create(<Button/>);
      const tree = testRenderer.toJSON();
      expect(tree).toHaveStyleRule(
        'box-shadow',
        DefaultTheme.button.normal.shadow);
    });

    it('should have the normal transition duration', () => {
      const testRenderer = renderer.create(<Button/>);
      const tree = testRenderer.toJSON();
      expect(tree).toHaveStyleRule(
        'transition-duration',
        DefaultTheme.button.normal.transition.duration);
    });
  });

  describe('in hover state', () => {
    it('should have the hover background color', () => {
      const testRenderer = renderer.create(<Button/>);
      const tree = testRenderer.toJSON();
      expect(tree).toHaveStyleRule(
        'background-color',
        DefaultTheme.button.hover.color.background,
        { modifier: ':hover' });
    });

    it('should have the hover border', () => {
      const testRenderer = renderer.create(<Button/>);
      const tree = testRenderer.toJSON();
      expect(tree).toHaveStyleRule(
        'border',
        DefaultTheme.button.hover.border,
        { modifier: ':hover' });
    });

    it('should have the hover border color', () => {
      const testRenderer = renderer.create(<Button/>);
      const tree = testRenderer.toJSON();
      expect(tree).toHaveStyleRule(
        'border-color',
        DefaultTheme.button.hover.color.border,
        { modifier: ':hover' });
    });

    it('should have the hover shadow', () => {
      const testRenderer = renderer.create(<Button/>);
      const tree = testRenderer.toJSON();
      expect(tree).toHaveStyleRule(
        'box-shadow',
        DefaultTheme.button.hover.shadow,
        { modifier: ':hover' });
    });

    it('should have the hover transition-duration', () => {
      const testRenderer = renderer.create(<Button/>);
      const tree = testRenderer.toJSON();
      expect(tree).toHaveStyleRule(
        'transition-duration',
        DefaultTheme.button.hover.transition.duration,
        { modifier: ':hover' });
    });
  });

  describe('in active state', () => {
    it('should have the active background color', () => {
      const testRenderer = renderer.create(<Button/>);
      const tree = testRenderer.toJSON();
      expect(tree).toHaveStyleRule(
        'background-color',
        DefaultTheme.button.active.color.background,
        { modifier: ':active' });
    });

    it('should have the active border', () => {
      const testRenderer = renderer.create(<Button/>);
      const tree = testRenderer.toJSON();
      expect(tree).toHaveStyleRule(
        'border',
        DefaultTheme.button.active.border,
        { modifier: ':active' });
    });

    it('should have the active border color', () => {
      const testRenderer = renderer.create(<Button/>);
      const tree = testRenderer.toJSON();
      expect(tree).toHaveStyleRule(
        'border-color',
        DefaultTheme.button.active.color.border,
        { modifier: ':active' });
    });

    it('should have the active shadow', () => {
      const testRenderer = renderer.create(<Button/>);
      const tree = testRenderer.toJSON();
      expect(tree).toHaveStyleRule(
        'box-shadow',
        DefaultTheme.button.active.shadow,
        { modifier: ':active' });
    });

    it('should have the active transition duration', () => {
      const testRenderer = renderer.create(<Button/>);
      const tree = testRenderer.toJSON();
      expect(tree).toHaveStyleRule(
        'transition-duration',
        DefaultTheme.button.active.transition.duration,
        { modifier: ':active' });
    });
  });
});
