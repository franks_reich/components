export const ComponentHighlightSubTitleTheme = {
  font: '"Montserrat",sans-serif',
  weight: 'normal',
  fontStyle: 'normal',
  size: '1.4em',
  color: {
    background: 'transparent',
    foreground: 'blue'
  }
};
