import React from 'react';

import styled from 'styled-components';
import { keyframes } from 'styled-components';

import { DefaultTheme } from "../../theme/DefaultTheme";


const rotation = keyframes`
  from { transform: rotate(0deg); }
  to { transform: rotate(360deg); }
`;

export const BusyOverlayIcon = styled.i`
  margin: ${ props => props.theme.overlayIcon.normal.margin };
  font-size: ${ props => props.theme.overlayIcon.normal.size };
  animation: ${ rotation } 2s linear infinite;
  color: ${ props => props.theme.overlayIcon.normal.color.foreground };
`;

BusyOverlayIcon.defaultProps = { theme: DefaultTheme };
