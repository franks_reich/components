import React from 'react';

import styled from 'styled-components';

import { RowContainer, ColumnContainer } from "../basic_layout/Container";
import { ComponentSubTitle } from "../basic_layout/Text";
import { Tile } from "./tile_group/Tile"


const TilesContainer = styled(RowContainer)`
  flex-wrap: wrap;
`;

export class TileGroup extends React.Component {
  render() {
    const title = this.props.title;
    const tiles = this.createTiles();
    return (
      <ColumnContainer>
        <RowContainer>
          <ComponentSubTitle>{title}</ComponentSubTitle>
        </RowContainer>
        <TilesContainer>
          { tiles }
        </TilesContainer>
      </ColumnContainer>
    );
  }

  createTiles() {
    const tiles = this.props.tiles;
    return tiles.map((tile, index) => (
      <Tile key={ index } tile={ tile } />
    ));
  }
}
