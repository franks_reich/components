import React from 'react';

import styled from 'styled-components';

import { DefaultTheme } from "../../theme/DefaultTheme";
import { ItemTitle, ItemSubTitle } from "../../basic_layout/Text";
import { RowContainer } from "../../basic_layout/Container";


export const TileTitle = styled(ItemTitle)`
  text-align: left;
`;

export const TileTitleContainer = styled(RowContainer)``;

export const TileSubTitle = styled(ItemSubTitle)`
  text-align: left;
  font-weight: bold;
`;

export const TileSubTitleContainer = styled(RowContainer)`
  padding-top: 0.5em;
  padding-left: 1.3em;
`;

export const TileBase = styled.div`
  background-color: ${ props => props.theme.tile.normal.color.background };
  border: ${ props => props.theme.tile.normal.border };
  border-color: ${ props => props.theme.tile.normal.color.border };
  width: ${ props => props.theme.tile.width };
  height: ${ props => props.theme.tile.height };
  margin: ${ props => props.theme.tile.margin };
  border-radius: ${ props => props.theme.tile.borderRadius };
  flex-shrink: 0;
  
  &:hover {
    background-color: ${ props => props.theme.tile.hover.color.background };
    border: ${ props => props.theme.tile.hover.border };
    border-color: ${ props => props.theme.tile.hover.color.border };
    box-shadow: ${ props => props.theme.tile.hover.shadow };
    transition-duration: ${ props => props.theme.tile.hover.transition.duration };
  }

  &:active {
    background-color: ${ props => props.theme.tile.active.color.background };
    border: ${ props => props.theme.tile.active.border };
    border-color: ${ props => props.theme.tile.active.color.border };
    box-shadow: ${ props => props.theme.tile.active.shadow };
    transition-duration: ${ props => props.theme.tile.active.transition.duration };
  }
`;

TileBase.defaultProps = { theme: DefaultTheme };
