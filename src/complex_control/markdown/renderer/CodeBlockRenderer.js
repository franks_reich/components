import React from 'react';

import hljs from 'highlight.js';

import { ContentContainer } from "../MarkdownContainer";


export class CodeBlockRenderer extends React.Component {
  constructor() {
    super();
    this.reference = null;

    this.setReference = this.setReference.bind(this);
  }

  setReference(reference) {
    this.reference = reference;
  }

  componentDidMount() {
    this.highlightCode();
  }

  componentDidUpdate() {
    this.highlightCode();
  }

  highlightCode() {
    hljs.highlightBlock(this.reference);
  }

  render() {
    return (
      <ContentContainer>
        <pre>
          <code
            ref={ this.setReference }
            className={ `language-${this.props.language}` }>
            { this.props.value }
          </code>
        </pre>
      </ContentContainer>
    );
  }
}

CodeBlockRenderer.defaultProps = { language: '' };

