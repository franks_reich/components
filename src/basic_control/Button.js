import React from 'react';

import styled from "styled-components";
import { curry } from 'ramda';

import {
  IconApplicationTitle, IconComponentTitle, IconComponentSubTitle, IconItemTitle,
  IconItemSubTitle, IconText
} from "./Icon";

import {
  ApplicationTitle, ComponentTitle, ComponentSubTitle, ItemSubTitle, ItemTitle, Text
} from "../basic_layout/Text";

import { DefaultTheme } from "../theme/DefaultTheme";

import { PaddingContainer, Spacer } from "../basic_layout/Container";


export const StyledButton = styled.button`
  margin: 0;
  padding: 0;
  background-color: ${ props => props.theme.button.normal.color.background };
  border: ${ props => props.theme.button.normal.border };
  border-color: ${props => props.theme.button.normal.color.border };
  box-shadow: ${props => props.theme.button.normal.shadow };
  transition-duration: ${ props => props.theme.button.normal.transition.duration };
  
  &:hover {
    background-color: ${ props => props.theme.button.hover.color.background };
    border: ${ props => props.theme.button.hover.border };
    border-color: ${ props => props.theme.button.hover.color.border };
    box-shadow: ${ props => props.theme.button.hover.shadow };
    transition-duration: ${ props => props.theme.button.hover.transition.duration };
  }
  
  &:active {
    background-color: ${ props => props.theme.button.active.color.background };
    border: ${ props => props.theme.button.active.border };
    border-color: ${ props => props.theme.button.active.color.border };
    box-shadow: ${ props => props.theme.button.active.shadow };
    transition-duration: ${ props => props.theme.button.active.transition.duration };
  }
`;

StyledButton.defaultProps = { theme: DefaultTheme };

const createText = (TextTag, text) => {
  if (text) {
    return (
      <TextTag>{ text }</TextTag>
    );
  } else {
    return null;
  }
};

const createIcon = (Icon, iconClass) => {
  if (iconClass) {
    return <Icon className={ iconClass } />;
  } else {
    return null;
  }
};

export const ButtonBase = (Button, Icon, Text, props) => {
  const icon = createIcon(Icon, props.iconClass);
  const text = createText(Text, props.text);
  const spacer = icon && text ? <Spacer height='0.5em'/> : null;
  const onClick = props.onClick ? props.onClick : () => { console.log("No handler") };
  return (
    <Button onClick={ onClick }>
      <PaddingContainer>
        { icon }
        { spacer }
        { text }
      </PaddingContainer>
    </Button>
  );
};

const ButtonBaseCurry = curry(ButtonBase);

export const ButtonApplicationTitle = ButtonBaseCurry(
  StyledButton, IconApplicationTitle, ApplicationTitle);
export const ButtonComponentTitle = ButtonBaseCurry(
  StyledButton, IconComponentTitle, ComponentTitle);
export const ButtonComponentSubTitle = ButtonBaseCurry(
  StyledButton, IconComponentSubTitle, ComponentSubTitle);
export const ButtonItemTitle = ButtonBaseCurry(
  StyledButton, IconItemTitle, ItemTitle);
export const ButtonItemSubTitle = ButtonBaseCurry(
  StyledButton, IconItemSubTitle, ItemSubTitle);
export const ButtonText = ButtonBaseCurry(
  StyledButton, IconText, Text);
