export const BoldTextTheme = {
  font: '"Montserrat",sans-serif',
  weight: 'bold',
  fontStyle: 'normal',
  size: '0.8em',
  color: {
    background: 'transparent',
    foreground: 'black'
  }
};
