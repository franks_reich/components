import React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components'

import {
  IconApplicationTitle, IconComponentTitle, IconComponentSubTitle, IconItemTitle,
  IconItemSubTitle, IconText, DefaultTheme
} from '../../src';


describe('The IconApplicationTitle', () => {
  it('should be an i', () => {
    const testRenderer = renderer.create(<IconApplicationTitle/>);
    testRenderer.root.findByType('i');
  });

  it('should have the font-size 2.2em', () => {
    const testRenderer = renderer.create(<IconApplicationTitle/>);
    const tree = testRenderer.toJSON();
    expect(tree).toHaveStyleRule('font-size', DefaultTheme.text.size.applicationTitle);
  });

  it('should have the margin set to auto', () => {
    const testRenderer = renderer.create(<IconApplicationTitle/>);
    const tree = testRenderer.toJSON();
    expect(tree).toHaveStyleRule('margin', 'auto');
  })
});

describe('The IconComponentTitle', () => {
  it('should be an i', () => {
    const testRenderer = renderer.create(<IconComponentTitle/>);
    testRenderer.root.findByType('i');
  });

  it('should have the font-size 2em', () => {
    const testRenderer = renderer.create(<IconComponentTitle/>);
    const tree = testRenderer.toJSON();
    expect(tree).toHaveStyleRule('font-size', DefaultTheme.text.size.componentTitle);
  });

  it('should have the margin set to auto', () => {
    const testRenderer = renderer.create(<IconApplicationTitle/>);
    const tree = testRenderer.toJSON();
    expect(tree).toHaveStyleRule('margin', 'auto');
  })
});

describe('The IconComponentSubTitle', () => {
  it('should be an i', () => {
    const testRenderer = renderer.create(<IconComponentSubTitle/>);
    testRenderer.root.findByType('i');
  });

  it('should have the font-size 1.8em', () => {
    const testRenderer = renderer.create(<IconComponentSubTitle/>);
    const tree = testRenderer.toJSON();
    expect(tree).toHaveStyleRule('font-size', DefaultTheme.text.size.componentSubTitle);
  });

  it('should have the margin set to auto', () => {
    const testRenderer = renderer.create(<IconComponentSubTitle/>);
    const tree = testRenderer.toJSON();
    expect(tree).toHaveStyleRule('margin', 'auto');
  })
});

describe('The IconItemTitle', () => {
  it('should be an i', () => {
    const testRenderer = renderer.create(<IconItemTitle/>);
    testRenderer.root.findByType('i');
  });

  it('should have the font-size 1.6em', () => {
    const testRenderer = renderer.create(<IconItemTitle/>);
    const tree = testRenderer.toJSON();
    expect(tree).toHaveStyleRule('font-size', DefaultTheme.text.size.itemTitle);
  });

  it('should have the margin set to auto', () => {
    const testRenderer = renderer.create(<IconItemTitle/>);
    const tree = testRenderer.toJSON();
    expect(tree).toHaveStyleRule('margin', 'auto');
  })
});

describe('The IconItemSubTitle', () => {
  it('should be an i', () => {
    const testRenderer = renderer.create(<IconItemSubTitle/>);
    testRenderer.root.findByType('i');
  });

  it('should have the font-size 1.4em', () => {
    const testRenderer = renderer.create(<IconItemSubTitle/>);
    const tree = testRenderer.toJSON();
    expect(tree).toHaveStyleRule('font-size', DefaultTheme.text.size.itemSubTitle);
  });

  it('should have the margin set to auto', () => {
    const testRenderer = renderer.create(<IconItemSubTitle/>);
    const tree = testRenderer.toJSON();
    expect(tree).toHaveStyleRule('margin', 'auto');
  })
});

describe('The IconText', () => {
  it('should be an i', () => {
    const testRenderer = renderer.create(<IconText/>);
    testRenderer.root.findByType('i');
  });

  it('should have the font-size 1em', () => {
    const testRenderer = renderer.create(<IconText/>);
    const tree = testRenderer.toJSON();
    expect(tree).toHaveStyleRule('font-size', DefaultTheme.text.size.text);
  });

  it('should have the margin set to auto', () => {
    const testRenderer = renderer.create(<IconText/>);
    const tree = testRenderer.toJSON();
    expect(tree).toHaveStyleRule('margin', 'auto');
  })
});
