import React from 'react';

import styled from 'styled-components';

import { DefaultTheme } from "../../theme/DefaultTheme";


export const OverlayBackground = styled.div`
  position: ${ props => props.theme.overlayBackground.normal.position.type };
  width: ${ props => props.theme.overlayBackground.normal.position.width };
  height: ${ props => props.theme.overlayBackground.normal.position.height };
  top: ${ props => props.theme.overlayBackground.normal.position.top };
  left: ${ props => props.theme.overlayBackground.normal.position.left };
  right: ${ props => props.theme.overlayBackground.normal.position.right };
  bottom: ${ props => props.theme.overlayBackground.normal.position.bottom };
  margin: ${ props => props.theme.overlayBackground.normal.margin };
  background-color: ${ props => props.theme.overlayBackground.normal.color.background };
  display: ${ props => props.theme.overlayBackground.normal.display };
  justify-content: ${ props => props.theme.overlayBackground.normal.justifyContent };
`;

OverlayBackground.defaultProps = { theme: DefaultTheme };
