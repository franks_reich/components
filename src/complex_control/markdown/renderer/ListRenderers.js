import React from 'react';

import { RowContainer } from "../../../basic_layout/Container";

import {
  UnorderedList, ListItem, OrderedList
} from "../../../basic_layout/List";


export const ListRenderer = (props) => {
  const ordered = props.ordered;
  const children = props.children;
  if (ordered) {
    return (
      <OrderedList>
        { children }
      </OrderedList>);
  } else {
    return (
      <UnorderedList>
        { children }
      </UnorderedList>);
  }
};

export const ListItemRenderer = (props) => {
  return <ListItem>{ props.children }</ListItem>
};
